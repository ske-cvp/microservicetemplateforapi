package net.muffin.servicename.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import net.muffin.framework.core.persistence.dao.CommonDao;
import net.muffin.framework.core.persistence.dao.MyBatisCommonDao;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * <p>
 * Default Database Configuration
 * </p>
 *
 * <ul>
 * <li>Created on : 2017. 5. 16.</li>
 * <li>Created by : sptek</li>
 * </ul>
 */
@Configuration
@PropertySource("classpath:/application.properties")
public class DatabaseConfig {

	@Value("${spring.datasource.driver-class-name}")
	String driverClassName;

	@Value("${spring.datasource.url}")
	String url;

	@Value("${spring.datasource.username}")
	String userName;

	@Value("${spring.datasource.password}")
	String passwd;

	@Autowired
	private ApplicationContext applicationContext;

	@Bean
	@ConfigurationProperties(prefix="spring.datasource.hikari")
	public HikariConfig hikariConfig() {

		HikariConfig dataSource = new HikariConfig();

		dataSource.setJdbcUrl(this.url);
		dataSource.setUsername(this.userName);
		dataSource.setPassword(this.passwd);
		dataSource.setDriverClassName(this.driverClassName);

		return dataSource;
	}

	@Bean
	public HikariDataSource dataSource() {
		HikariDataSource dataSource = new HikariDataSource(hikariConfig());
		return dataSource;
	}

	@Bean
	public PlatformTransactionManager transactionManager(HikariDataSource dataSource) {
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);
		transactionManager.setGlobalRollbackOnParticipationFailure(false);
		return transactionManager;
	}

	@Bean
	public SqlSessionFactory sqlSessionFactory(HikariDataSource dataSource) throws Exception {
		SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
		sessionFactoryBean.setDataSource(dataSource);
		sessionFactoryBean
				.setConfigLocation(this.applicationContext.getResource("classpath:/mybatis/mybatis-config.xml"));
		sessionFactoryBean.setMapperLocations(
				this.applicationContext.getResources("classpath*:/net/muffin/**/sqlmap/*Mapper.xml"));
		sessionFactoryBean.setTypeAliasesPackage("net.muffin.servicename.entity");
		return sessionFactoryBean.getObject();
	}

	@Bean
	public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
		SqlSessionTemplate sessionTemplate = new SqlSessionTemplate(sqlSessionFactory);
		return sessionTemplate;
	}

	@Bean
	public CommonDao commonDao() {
		return new MyBatisCommonDao();
	}

}