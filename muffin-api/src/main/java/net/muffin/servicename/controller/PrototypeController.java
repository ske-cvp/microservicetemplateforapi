package net.muffin.servicename.controller;

import net.muffin.servicename.dto.PageInfoDto;
import net.muffin.servicename.exceptionHandler.MuffinDataNotFoundException;
import net.muffin.servicename.model.SearchUserReq;
import net.muffin.servicename.model.SearchUserRes;
import net.muffin.servicename.model.UserJoinReq;
import net.muffin.servicename.model.UserModifyReq;
import net.muffin.servicename.service.PrototypeService;
import net.muffin.servicename.type.OrderBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Validated
public class PrototypeController {

    private enum ApiVersion {
        v1, v2
    }

    private PrototypeService prototypeService;

    @Autowired
    public void setPrototypeService(PrototypeService prototypeService){
        this.prototypeService = prototypeService;
    }

    @RequestMapping(path = "/{api_version}/user/{user_seq}", method = RequestMethod.HEAD)
	public ResponseEntity<?> existUser(@PathVariable("api_version") ApiVersion apiVersion
            , @PathVariable("user_seq") Integer userSeq)  {

		return Optional.ofNullable(prototypeService.getUser(userSeq))
				.map(user -> ResponseEntity.noContent().build())
                .orElseThrow(MuffinDataNotFoundException::new);
	}

    @RequestMapping(path="/{api_version}/user", method = RequestMethod.OPTIONS)
    public ResponseEntity<?> options(@PathVariable("api_version") ApiVersion apiVersion)  {
        return ResponseEntity.ok()
                .allow(HttpMethod.HEAD
                        , HttpMethod.OPTIONS
                        , HttpMethod.GET
                        , HttpMethod.POST
                        , HttpMethod.PUT
                        , HttpMethod.DELETE
                        , HttpMethod.PATCH)
                .build();
    }


    @GetMapping("/{api_version}/user/{user_seq}")
    public ResponseEntity<?> getUser(@PathVariable("api_version") ApiVersion apiVersion
            , @PathVariable("user_seq") Integer userSeq)  {

        return Optional.ofNullable(prototypeService.getUser(userSeq))
                .map(ResponseEntity::ok)
                .orElseThrow(RuntimeException::new);
    }

    @GetMapping("/{api_version}/userlist")
    public ResponseEntity<List<SearchUserRes>> getUserList(@PathVariable("api_version") ApiVersion apiVersion
            , @Validated @ModelAttribute SearchUserReq searchUserReq
            , @RequestParam(name = "start", defaultValue = "0", required = false) Integer start
            , @RequestParam(name = "limit", defaultValue = "10", required = false) Integer limit
            , @RequestParam(name = "order", defaultValue = "desc", required = false) OrderBy orderBy) {

        return Optional.ofNullable(new PageInfoDto<SearchUserReq>(searchUserReq, start, limit, orderBy))
                .map(dto -> prototypeService.getUserList(dto))
                .map(res -> ResponseEntity.ok().body(res))
                .orElseGet(()->  ResponseEntity.ok(Collections.emptyList()));
    }

    @PostMapping("/{api_version}/user/")
    public ResponseEntity<?> joinUser(@PathVariable("api_version") ApiVersion apiVersion
            , @Validated @RequestBody UserJoinReq userJoinReq) {

        URI uri = URI.create(
                ServletUriComponentsBuilder.fromCurrentRequest().toUriString()
        );

        return Optional.ofNullable(prototypeService.joinUser(userJoinReq))
                .map(res -> ResponseEntity.created(uri).body(res))
                .orElseThrow(MuffinDataNotFoundException::new);

    }

    @PutMapping("/{api_version}/user")
    public ResponseEntity<?> modifyUser(@PathVariable("api_version") ApiVersion apiVersion
            , @Validated @RequestBody UserModifyReq userModifyReq) {

        return Optional.ofNullable(prototypeService.modifyUser(userModifyReq))
                .map(res -> ResponseEntity.created(URI.create("/user/" + res.getUserSeq())).body(res))
                .orElseThrow(MuffinDataNotFoundException::new);
    }

    @PatchMapping("/{api_version}/user")
    public ResponseEntity<?> patchUser(@PathVariable("api_version") ApiVersion apiVersion
            , @Validated @RequestBody UserModifyReq userModifyReq) {

        return Optional.of(prototypeService.patchUser(userModifyReq))
                .map(res -> ResponseEntity.ok().body(res))
                .orElseThrow(MuffinDataNotFoundException::new);
    }

    @DeleteMapping("/{api_version}/user/{user_seq}")
    public ResponseEntity<?> withdrawUser(@PathVariable("api_version") ApiVersion apiVersion
            , @PathVariable("user_seq") Integer userSeq) {

        if(prototypeService.deleteUser(userSeq)) {
            return ResponseEntity.noContent().build();
        } else {
            throw new MuffinDataNotFoundException();
        }

    }
}
