package net.muffin.servicename.dto;

import lombok.*;
import net.muffin.servicename.type.OrderBy;


@Getter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class PageInfoDto<T> {
	@NonNull
	T paramData;

	int start;
	int limit;

	@NonNull
	OrderBy orderBy;
}
