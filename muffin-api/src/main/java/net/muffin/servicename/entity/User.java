package net.muffin.servicename.entity;

import lombok.*;
import net.muffin.servicename.type.Yn;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {

	Integer userSeq;

	String userPhone;

	String email;

	int age;

	String userId;

	String gender;

	Date joinDt;
}
