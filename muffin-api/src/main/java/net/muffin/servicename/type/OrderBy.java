package net.muffin.servicename.type;

public enum OrderBy {
    asc("asc"),
    desc("desc");

    String value;

    OrderBy(String value) {
        this.value = value;
    }

    public String getName() {
        return this.value;
    }

}
