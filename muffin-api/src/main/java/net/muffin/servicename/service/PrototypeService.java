package net.muffin.servicename.service;

import net.muffin.servicename.dto.PageInfoDto;
import net.muffin.servicename.model.*;

import java.util.List;

public interface PrototypeService {

    UserRes getUser(Integer userSeq);

    UserJoinRes joinUser(UserJoinReq userJoinReq);

    UserModifyRes modifyUser(UserModifyReq UserModifyReq);

    List<SearchUserRes> getUserList(PageInfoDto<SearchUserReq> condition);

    Boolean deleteUser(Integer userSeq);

    UserModifyRes patchUser(UserModifyReq userModifyReq);
}
