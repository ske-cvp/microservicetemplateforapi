package net.muffin.servicename.service.impl;

import net.muffin.servicename.dao.PrototypeDao;
import net.muffin.servicename.dto.PageInfoDto;
import net.muffin.servicename.entity.User;
import net.muffin.servicename.model.*;
import net.muffin.servicename.service.PrototypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PrototypeServiceImpl implements PrototypeService {

    private PrototypeDao prototypeDao;

    @Autowired
    public void setProtoTypeDao(PrototypeDao protoTypeDao) {
        this.prototypeDao = protoTypeDao;
    }
    @Override
    public UserRes getUser(Integer userSeq) {
        return Optional.ofNullable(prototypeDao.findUserById(userSeq))
                .map(r -> UserRes.builder()
                        .userSeq(r.getUserSeq())
                        .age(r.getAge())
                        .email(r.getEmail())
                        .gender(r.getGender())
                        .joinDt(r.getJoinDt())
                        .userId(r.getUserId())
                        .userPhone(r.getUserPhone())
                        .build())
                .orElse(null);
    }

    @Override
    public UserJoinRes joinUser(UserJoinReq userJoinReq) {
        User user = prototypeDao.createUser(User.builder()
                .userId(userJoinReq.getUserId())
                .email(userJoinReq.getEmail())
                .userPhone(userJoinReq.getUserPhone())
                .gender(userJoinReq.getGender())
                .build()
        );

        UserJoinRes userJoinRes = UserJoinRes.builder()
                .userSeq(user.getUserSeq())
                .build();

        return userJoinRes;
    }

    @Override
    public UserModifyRes modifyUser(UserModifyReq userModifyReq) {

        return Optional.ofNullable(prototypeDao.findUserById(userModifyReq.getUserSeq()))
                .map(user -> {
                    user.setUserId(userModifyReq.getUserId());
                    user.setEmail(userModifyReq.getEmail());
                    user.setUserPhone(userModifyReq.getUserPhone());
                    user.setGender(userModifyReq.getGender());

                    prototypeDao.modifyUser(user);

                    return user;
                }).map(user -> UserModifyRes.builder()
                        .userSeq(user.getUserSeq())
                        .userId(user.getUserId())
                        .email(user.getEmail())
                        .userPhone(user.getUserPhone())
                        .gender(user.getGender())
                        .build()
                ).orElse(null);
    }

    @Override
    public List<SearchUserRes> getUserList(PageInfoDto<SearchUserReq> condition) {

        return prototypeDao.findUsers(condition).stream()
                .map(user -> {
                    return SearchUserRes.builder()
                            .userSeq(user.getUserSeq())
                            .userId(user.getUserId())
                            .email(user.getEmail())
                            .userPhone(user.getUserPhone())
                            .gender(user.getGender())
                            .build();
                }).collect(Collectors.toList());
    }

    @Override
    public Boolean deleteUser(Integer userSeq) {
        return prototypeDao.deleteUser(userSeq);
    }

    @Override
    public UserModifyRes patchUser(UserModifyReq userModifyReq) {
        return this.modifyUser(userModifyReq);
    }

}
