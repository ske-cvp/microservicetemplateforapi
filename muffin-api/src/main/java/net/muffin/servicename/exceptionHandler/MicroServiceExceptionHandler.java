package net.muffin.servicename.exceptionHandler;

import net.muffin.framework.core.exception.*;
import net.muffin.servicename.model.ErrorInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.BindException;
import java.util.Locale;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class MicroServiceExceptionHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(MicroServiceExceptionHandler.class);
	
	Locale locale = Locale.KOREAN; 
	
	MessageSourceAccessor messageSourceAccessor;

	@Autowired
	public void setMessageSourceAccessor(MessageSourceAccessor messageSourceAccessor) {
		this.messageSourceAccessor = messageSourceAccessor;
	}

	@ExceptionHandler({BindException.class
			, HttpMessageNotReadableException.class
			, MethodArgumentNotValidException.class
			, HttpMediaTypeNotSupportedException.class
			, MissingServletRequestPartException.class
			, TypeMismatchException.class
			, HttpMediaTypeNotSupportedException.class
			, MuffinXssInvalidException.class
			, MuffinMethodArgumentNotValidException.class})
	@ResponseBody
	public ErrorInfo error400(HttpServletRequest req, HttpServletResponse res, Exception ex) {
		logger.debug("error" ,ex);
		
		HttpStatus status = HttpStatus.BAD_REQUEST;
		res.setStatus(status.value());
		return getErrorInfo(status, ex);
	}
	
	@ExceptionHandler({MuffinDataNotFoundException.class/*NoSuchRequestHandlingMethodException.class*/})
	@ResponseBody
	public ErrorInfo error404(HttpServletRequest req, HttpServletResponse res, Exception ex) {
		logger.debug("error" ,ex);
		HttpStatus status = HttpStatus.NOT_FOUND;
		res.setStatus(status.value());
		return getErrorInfo(status, ex);
	}

	@ExceptionHandler({MuffinLogoutException.class})
	@ResponseBody
	public ErrorInfo error401(HttpServletRequest req, HttpServletResponse res, Exception ex) {
		logger.debug("error" ,ex);
		HttpStatus status = HttpStatus.UNAUTHORIZED;
		res.setStatus(status.value());
		return getErrorInfo(status, ex);
	}
	
	@ExceptionHandler({HttpMediaTypeNotAcceptableException.class})
	@ResponseBody
	public ErrorInfo error406(HttpServletRequest req, HttpServletResponse res, Exception ex) {
		logger.debug("error" ,ex);
		HttpStatus status = HttpStatus.METHOD_NOT_ALLOWED;
		res.setStatus(status.value());
		return getErrorInfo(status, ex);
	}
	
	@ExceptionHandler({ConversionNotSupportedException.class
			, HttpMessageNotWritableException.class
			, MuffinDataAccessException.class
			, MuffinNetworkException.class})
	@ResponseBody
	public ErrorInfo error500(HttpServletRequest req, HttpServletResponse res, Exception ex) {
		logger.debug("error" ,ex);
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		res.setStatus(status.value());
		return getErrorInfo(status, ex);
	}
	
	@ExceptionHandler({Exception.class})
	@ResponseBody
	public ErrorInfo errorEtc(HttpServletRequest req, HttpServletResponse res, Exception ex) {
		logger.debug("error" ,ex);
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		res.setStatus(status.value());
		return getErrorInfo(status, ex);
	}	
	
	private ErrorInfo getErrorInfo(HttpStatus status, Exception ex) {

		logger.debug("error", ex);
		String msg = messageSourceAccessor.getMessage("HTTP_STATUS_" + status.value(), locale);
		return ErrorInfo.builder().statusCode("" + status.value()).message(msg).code("" + status.value()).moreInfo("").build();
	}
}
