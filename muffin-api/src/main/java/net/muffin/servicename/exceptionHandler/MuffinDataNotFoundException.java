package net.muffin.servicename.exceptionHandler;

import net.muffin.framework.core.exception.MuffinException;
import net.muffin.framework.core.exception.domain.ErrorInfo;

public class MuffinDataNotFoundException extends MuffinException {
    public MuffinDataNotFoundException() {
        super("404");
    }
}
