package net.muffin.servicename.dao;

import net.muffin.servicename.dto.PageInfoDto;
import net.muffin.servicename.entity.User;

import java.util.List;

public interface PrototypeDao {

    User findUserById(Integer userSeq);

    List<User> findUsers(PageInfoDto<?> searchUserDto);

    User modifyUser(User user);

    Boolean deleteUser(Integer userSeq);

    User createUser(User user);
}
