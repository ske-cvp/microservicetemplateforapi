package net.muffin.servicename.dao.impl;

import net.muffin.framework.core.persistence.dao.CommonDao;
import net.muffin.servicename.dao.PrototypeDao;
import net.muffin.servicename.dto.PageInfoDto;
import net.muffin.servicename.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PrototypeDaoImpl implements PrototypeDao {
    public static final String MAPPER = "PrototypeMapper.";

    private CommonDao commonDao;

    @Autowired
    public void setCommonDao(CommonDao commonDao) {
        this.commonDao = commonDao;
    }


    @Override
    public User findUserById(Integer userSeq) {
        return commonDao.queryForObject(MAPPER + "selectUser", userSeq, User.class);
    }

    @Override
    public List<User> findUsers(PageInfoDto<?> searchUserDto) {
        System.out.println(searchUserDto.toString());
        return commonDao.queryForList(MAPPER + "selectUserList", searchUserDto, User.class);
    }

    @Override
    public User modifyUser(User user) {
        int cnt = commonDao.update(MAPPER + "modifyUser", user);
        return cnt > 0 ? user : null;
    }

    @Override
    public Boolean deleteUser(Integer userSeq) {
        int cnt = commonDao.delete(MAPPER + "deleteUser", userSeq);
        return cnt > 0;
    }

    @Override
    public User createUser(User user) {
        commonDao.insert(MAPPER + "insertUser", user);
        return user;
    }
}
