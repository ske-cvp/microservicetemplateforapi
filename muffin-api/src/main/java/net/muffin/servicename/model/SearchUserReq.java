package net.muffin.servicename.model;

import lombok.*;
import net.muffin.servicename.type.Yn;

import javax.validation.constraints.*;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchUserReq {
    @PositiveOrZero
    Integer userSeq;

    @Pattern(regexp = "[0-9]{10,11}")
    String userPhone;

    @NotEmpty
    @Email
    String email;

    @Min(0)
    @Max(200)
    int age;

    @Size(min = 5, max=11)
    String userId;

    String gender;

    @PastOrPresent
    Date joinDt;
}
