package net.muffin.servicename.model;

import lombok.Builder;
import lombok.Data;
import net.muffin.servicename.type.Yn;

import javax.validation.constraints.*;

@Data
@Builder
public class UserJoinReq {

    @Pattern(regexp = "[0-9]{10,11}")
    String userPhone;

    @NotEmpty
    @Email
    String email;

    @Min(0)
    @Max(200)
    int age;

    @Size(min = 5, max=11)
    String userId;

    String gender;
}
