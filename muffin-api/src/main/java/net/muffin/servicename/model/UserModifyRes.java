package net.muffin.servicename.model;

import lombok.Builder;
import lombok.Data;
import net.muffin.servicename.type.Yn;

import javax.validation.constraints.*;
import java.util.Date;

@Data
@Builder
public class UserModifyRes {
    Integer userSeq;

    String userPhone;

    String email;

    int age;

    String userId;

    String gender;

    Date joinDt;
}
