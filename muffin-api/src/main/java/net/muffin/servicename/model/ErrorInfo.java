package net.muffin.servicename.model;

import lombok.*;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ErrorInfo {
	
	String statusCode;
	
	String code;
	
	String message;
	
	String moreInfo;
}
