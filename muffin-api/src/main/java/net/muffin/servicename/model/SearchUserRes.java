package net.muffin.servicename.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.muffin.servicename.type.Yn;

import javax.validation.constraints.*;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchUserRes {
    Integer userSeq;

    String userPhone;

    String email;

    int age;

    String userId;

    String gender;

    Date joinDt;
}
