package net.muffin.api;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={MicroServiceApplicationTests.class})
public class MicroServiceApplicationTests {

	@Test
	@Ignore
	public void contextLoads() {

	}

}
