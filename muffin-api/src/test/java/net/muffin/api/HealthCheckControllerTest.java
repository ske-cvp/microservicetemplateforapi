package net.muffin.api;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@Transactional
public class HealthCheckControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @Ignore
    public void healthCheck() {
        assert(true);
    }
}
