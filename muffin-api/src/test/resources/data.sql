-- --------------------------------------------------------
-- 호스트:                          127.0.0.1
-- 서버 버전:                        10.1.23-MariaDB - mariadb.org binary distribution
-- 서버 OS:                        Win64
-- HeidiSQL 버전:                  9.4.0.5125
-- --------------------------------------------------------


-- 테이블 데이터 localtest.tbl_user:~3 rows (대략적) 내보내기
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`user_seq`, `user_id`, `email`, `gender`,`join_dt`, `upd_dt`, `reg_dt`) VALUES
	(1, 'acer07', 'acer07@nave.com', 'M', '2020-07-24 18:27:02', '2020-07-24 18:27:02', '2020-07-24 18:27:02'),
	(2, 'acer07154', 'acer07154@sptek.co.kr', 'M', '2020-07-24 18:27:02', '2020-07-24 18:27:02', '2020-07-24 18:27:02'),
	(3, 'acerzero','acerzero@nate.com', 'M', '2020-07-24 18:27:02', '2020-07-24 18:27:02', '2020-07-24 18:27:02');

/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;


/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
