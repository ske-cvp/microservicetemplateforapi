package net.muffin.framework.core.context;

public enum ResultStatus {
	SUCCESS, FAIL, IN_PROCESSING, SESSION_EXPIRED
}
