package net.muffin.framework.core.exception;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class DefaultMuffinErrorCodeTranslator extends MuffinErrorCodeTranslator {

	public DefaultMuffinErrorCodeTranslator() {
		super();
	}

	@Override
	public String translate(String packagee) {

		List<String> splitedPackageList = Arrays.asList(StringUtils.split(packagee, "."));

		String result;

		for (int index = splitedPackageList.size() - 1, min = 0; index >= min; index--) {
			result = this.errorCodeMap.get(StringUtils.join(splitedPackageList.subList(0, index), "."));

			if (result != null) {
				return result;
			}
		}

		return null;
	}

}
