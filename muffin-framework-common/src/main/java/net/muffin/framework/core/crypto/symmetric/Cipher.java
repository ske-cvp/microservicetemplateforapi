package net.muffin.framework.core.crypto.symmetric;

import static javax.crypto.Cipher.DECRYPT_MODE;
import static javax.crypto.Cipher.ENCRYPT_MODE;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.util.Arrays;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Cipher {

	private static final Logger logger = LoggerFactory.getLogger(Cipher.class);

	/** default 암호화 키 (properties 설정) */
	public static final String KEY_DEFAULT_CIPHER_KEY = "cipher.symm.default.key";

	/** default IV 키 (properties 설정) */
	public static final String KEY_DEFAULT_CIPHER_IV = "cipher.symm.default.iv";

	/** 암호화 키 Pool (properties 설정) */
	public static final String KEY_POOL_CIPHER_KEY = "cipher.symm.pool.key";

	/** IV 키 Pool (properties 설정) */
	public static final String KEY_POOL_CIPHER_IV = "cipher.symm.pool.iv";

	/** default transformation (properties 설정) */
	public static final String KEY_DEFAULT_CIPHER_TRANSFORMATION = "cipher.symm.default.transformation";

	/** default 알고리즘 (properties 설정) */
	public static final String KEY_DEFAULT_CIPHER_ALGORITHM = "cipher.symm.default.algorithm";

	/** default transformation */
	public static final String DEFAULT_TRANSFORMATION = "AES/CTR/NoPadding";

	/** default 암호화 key */
	public static final byte[] DEFAULT_KEY_BYTES = new byte[] { (byte) 0x82, (byte) 0xaf, (byte) 0x9e, (byte) 0xbe,
			(byte) 0x80, (byte) 0x24, (byte) 0xd4, (byte) 0x18, (byte) 0x2c, (byte) 0xdf, (byte) 0x6e, (byte) 0x90,
			0x16, (byte) 0x44, (byte) 0x3e, (byte) 0xd6 };

	/** default iv 키 */
	public static final byte[] DEFAULT_IV_BYTES = new byte[] { (byte) 0xa2, 0x6b, (byte) 0x95, (byte) 0xe2, 0x54,
			(byte) 0x30, (byte) 0x8c, 0x4d, (byte) 0x7e, 0x69, (byte) 0x2e, (byte) 0xc7, 0x3d, (byte) 0x1d, (byte) 0xd5,
			(byte) 0x27 };

	/** 암복호화 provider */
	private static final String CIPHER_PROVIDER = "BC";

	/** default 알고리즘 */
	private static final String DEFAULT_ALGORITHM = "AES";

	/** key Delimiter */
	private static final String CIPHER_KEY_DELIMITER = ",";

	/** key Pool Size */
	public static final int CIPHER_KEY_POOL_SIZE = 100;

	/** key Bytes */
	private static byte[] keyBytes = DEFAULT_KEY_BYTES;

	/** iv Bytes */
	private static byte[] ivBytes = DEFAULT_IV_BYTES;

	/** key Bytes Pool */
	private static byte[][] keyBytesPool = new byte[CIPHER_KEY_POOL_SIZE][];

	/** key Bytes Pool */
	private static byte[][] ivBytesPool = new byte[CIPHER_KEY_POOL_SIZE][];

	/** algorithm */
	private static String algorithm = DEFAULT_ALGORITHM;

	/** transformation */
	private static String transformation = DEFAULT_TRANSFORMATION;

	/** 암호화 클래스 */
	private javax.crypto.Cipher encrypter = null;

	/** 복호화 클래스 */
	private javax.crypto.Cipher decrypter = null;

	/** Base64 인코딩 여부 */
	private boolean useBase64 = false;

	static {
		Initialize();
	}

	/**
	 * <pre>
	 * 암복호화를 위한 기반 정보 환경별 초기화.
	 * </pre>
	 */
	public static synchronized void Initialize() {
		if (Security.getProvider(CIPHER_PROVIDER) == null) { // initialize the provider.
			Security.addProvider(new BouncyCastleProvider());
		}
		String profile = System.getProperty("spring.profiles.active");
		if (StringUtils.isEmpty(profile) || "test".equals(profile))
			profile = "local";

		Properties prop = new Properties();
//		String propFileName = "properties/application-" + profile + ".properties";
		String propFileName = "properties/cipher." + profile + ".properties";

		InputStream inputStream = Cipher.class.getClassLoader().getResourceAsStream(propFileName);

		if (inputStream != null) {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				logger.error("There is a problem of the " + propFileName, e);
			}
		}
		logger.info("loaded cipher properties = {}", propFileName);

		// 대칭키 설정
		if (prop.get(KEY_DEFAULT_CIPHER_KEY) != null) {
			byte[] propKeyBytes = Hex.decode(prop.getProperty(KEY_DEFAULT_CIPHER_KEY));
			keyBytes = Arrays.copyOf(propKeyBytes, propKeyBytes.length);
		}

		// Initail Vector 설정
		if (prop.get(KEY_DEFAULT_CIPHER_IV) != null) {
			byte[] propIvBytes = Hex.decode(prop.getProperty(KEY_DEFAULT_CIPHER_IV));
			ivBytes = Arrays.copyOf(propIvBytes, propIvBytes.length);
		}

		// 대칭키 Pool 설정
		if (prop.get(KEY_POOL_CIPHER_KEY) != null) {
			String[] keyPool = ((String) prop.get(KEY_POOL_CIPHER_KEY)).split(CIPHER_KEY_DELIMITER);
			for (int i = 0; i < keyPool.length; i++) {
				byte[] keyBytes = Hex.decode(keyPool[i]);
				keyBytesPool[i] = Arrays.copyOf(keyBytes, keyBytes.length);
			}
		}

		// Initail Vector Pool 설정
		if (prop.get(KEY_POOL_CIPHER_IV) != null) {
			String[] ivPool = ((String) prop.get(KEY_POOL_CIPHER_IV)).split(CIPHER_KEY_DELIMITER);
			for (int i = 0; i < ivPool.length; i++) {
				byte[] ivBytes = Hex.decode(ivPool[i]);
				ivBytesPool[i] = Arrays.copyOf(ivBytes, ivBytes.length);
			}
		}

		// Algorithm 설정
		if (prop.get(KEY_DEFAULT_CIPHER_ALGORITHM) != null) {
			algorithm = (String) prop.get(KEY_DEFAULT_CIPHER_ALGORITHM);
		}

		// 변환 설정
		if (prop.get(KEY_DEFAULT_CIPHER_TRANSFORMATION) != null) {
			transformation = (String) prop.get(KEY_DEFAULT_CIPHER_TRANSFORMATION);
		}

		// 최초 getInstance 호출 시 로딩 시간에 대한 Delay를 방지하기 위하여 선 호출 처리
		try {
			javax.crypto.Cipher.getInstance(Cipher.transformation, CIPHER_PROVIDER);
		} catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			logger.error("", e);
		}
	}

	/**
	 *
	 * <pre>
	 * Create the "AES/CTR/NoPadding" cipher object by the default key and the default iv.
	 * </pre>
	 *
	 * @throws GeneralSecurityException
	 *             exception of creating the cipher.
	 */
	public Cipher() throws GeneralSecurityException {
		this(keyBytes, algorithm, ivBytes, transformation, true);
	}

	/**
	 *
	 * <pre>
	 * Create the "AES/CTR/NoPadding" cipher object by the default key and the default iv.
	 * </pre>
	 *
	 * @param useBase64
	 *            - use base64 encoding (after encryption, before decryption). if this value is false, use hex encoding
	 *
	 * @throws GeneralSecurityException
	 *             exception of creating the cipher.
	 */
	public Cipher(boolean useBase64) throws GeneralSecurityException {
		this(keyBytes, algorithm, ivBytes, transformation, useBase64);
	}

	public Cipher(int idx) throws GeneralSecurityException {
		this(keyBytesPool[idx], algorithm, ivBytesPool[idx], transformation, false);
	}

	/**
	 *
	 * <pre>
	 * Create the "AES/CTR/NoPadding" cipher object by the default key and the default iv.
	 * </pre>
	 *
	 * @param useBase64
	 *            - use base64 encoding (after encryption, before decryption). if this value is false, use hex encoding
	 *
	 * @throws GeneralSecurityException
	 *             exception of creating the cipher.
	 */
	public Cipher(String profile, boolean useBase64) throws GeneralSecurityException {
		this(keyBytes, algorithm, ivBytes, transformation, useBase64);
	}

	/**
	 *
	 * <pre>
	 * Create the "AES/CTR/NoPadding" cipher object by the default key and the default iv.
	 * </pre>
	 *
	 * @throws GeneralSecurityException
	 *             exception of creating the cipher.
	 */
	public Cipher(byte[] keyBytes) throws GeneralSecurityException {
		this(keyBytes, algorithm, ivBytes, transformation, true);
	}

	/**
	 *
	 * <pre>
	 * Create the "AES/CTR/NoPadding" cipher object by the default key and the default iv.
	 * </pre>
	 *
	 * @param keyBytes
	 *            - bytes of key string.
	 * @param ivBytes
	 *            - bytes of initial values.
	 * @param useBase64
	 *            - use base64 encoding (after encryption, before decryption). if this value is false, use hex encoding.
	 * @throws GeneralSecurityException
	 *             exception of creating the cipher.
	 */
	public Cipher(byte[] keyBytes, boolean useBase64) throws GeneralSecurityException {
		this(keyBytes, algorithm, ivBytes, transformation, useBase64);
	}

	/**
	 *
	 * <pre>
	 * Create the "AES/CTR/NoPadding" cipher object by given the key and the iv.
	 * </pre>
	 *
	 * @param keyBytes
	 *            - bytes of key string.
	 * @param ivBytes
	 *            - bytes of initial values.
	 * @throws GeneralSecurityException
	 *             exception of creating the cipher.
	 */
	public Cipher(byte[] keyBytes, byte[] ivBytes) throws GeneralSecurityException {
		this(keyBytes, algorithm, ivBytes, transformation, true);
	}

	/**
	 *
	 * <pre>
	 * Create the "AES/CTR/NoPadding" cipher object by given the key and the iv.
	 * </pre>
	 *
	 * @param keyBytes
	 *            - bytes of key string.
	 * @param ivBytes
	 *            - bytes of initial values.
	 * @param useBase64
	 *            - use base64 encoding (after encryption, before decryption). if this value is false, use hex encoding.
	 * @throws GeneralSecurityException
	 *             exception of creating the cipher.
	 */
	public Cipher(byte[] keyBytes, byte[] ivBytes, boolean useBase64) throws GeneralSecurityException {
		this(keyBytes, algorithm, ivBytes, transformation, useBase64);
	}

	/**
	 *
	 * <pre>
	 * Create the cipher object by given the key, algorithm, iv and the transformation.
	 * </pre>
	 *
	 * @param keyBytes
	 *            - bytes of key string.
	 * @param algorithm
	 *            - encryption/decryption method (ex: AES).
	 * @param ivBytes
	 *            - bytes of initial values.
	 * @param transformation
	 *            - string of transformation for the encryption/decryption (ex: AES/CTR/NoPadding).
	 * @throws GeneralSecurityException
	 *             exception of creating the cipher.
	 */
	public Cipher(byte[] keyBytes, String algorithm, byte[] ivBytes, String transformation, boolean useBase64)
			throws GeneralSecurityException {
		Cipher.keyBytes = Arrays.copyOf(keyBytes, keyBytes.length);
		Cipher.ivBytes = Arrays.copyOf(ivBytes, ivBytes.length);
		Cipher.transformation = transformation;
		Cipher.algorithm = algorithm;
		this.useBase64 = useBase64;
		this.initCipher();
	}

	/*
	 *
	 * Initialize the {@Cipher}.
	 *
	 * @throws NoSuchAlgorithmException - if you use the invalid algorithm.
	 *
	 * @throws NoSuchProviderException - when there is no security provider
	 *
	 * @throws NoSuchPaddingException - if you use the invalid padding.
	 *
	 * @throws InvalidKeyException - if you use the invalid key.
	 *
	 * @throws InvalidAlgorithmParameterException if you use the invalid iv.
	 */
	private void initCipher() throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException {
		SecretKeySpec keySpec = new SecretKeySpec(Cipher.keyBytes, Cipher.algorithm);
		IvParameterSpec ivSpec = new IvParameterSpec(Cipher.ivBytes);

		this.encrypter = javax.crypto.Cipher.getInstance(Cipher.transformation, CIPHER_PROVIDER);
		this.decrypter = javax.crypto.Cipher.getInstance(Cipher.transformation, CIPHER_PROVIDER);

		this.encrypter.init(ENCRYPT_MODE, keySpec, ivSpec);
		this.decrypter.init(DECRYPT_MODE, keySpec, ivSpec);
	}

	/**
	 *
	 * <pre>
	 * Retrieve the encrypted data of given plain text.
	 * </pre>
	 *
	 * @param data
	 *            - a plain text.
	 * @return an encrypted data.
	 * @throws IllegalBlockSizeException
	 *             When the given data have a illegal block size.
	 * @throws BadPaddingException
	 *             When Padding is failed.
	 * @throws UnsupportedEncodingException
	 */
	public String encrypt(String data) throws IllegalBlockSizeException, BadPaddingException {
		if (StringUtils.isEmpty(data))
			return null;

		byte[] encryptedData = this.encrypter.doFinal(data.getBytes());

		return new String((this.useBase64 ? Base64.encode(encryptedData) : Hex.encode(encryptedData)));
	}

	/**
	 *
	 * <pre>
	 * Retrieve the decrypted data of given encrypted data.
	 * </pre>
	 *
	 * @param data
	 *            - an encrypted data.
	 * @return a decrypted data.
	 * @throws IllegalBlockSizeException
	 *             When the given data have a illegal block size.
	 * @throws BadPaddingException
	 *             When Padding is failed.
	 * @throws UnsupportedEncodingException
	 */
	public String decrypt(String data) throws IllegalBlockSizeException, BadPaddingException {
		if (StringUtils.isEmpty(data))
			return null;

		byte[] decryptedData = this.useBase64 ? Base64.decode(data) : Hex.decode(data);

		return new String(this.decrypter.doFinal(decryptedData));
	}

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println(
					"[USAGE]	java -Dspring.profiles.active=local com.muffin.framework.core.crypto.symmetric.Cipher [mode] [Text]");
			System.out.println("			[mode] : operation mode");
			System.out.println("					-db / decrypted & base64");
			System.out.println("					-eb / encrypted & base64");
			System.out.println("					-dh / decrypted & hex");
			System.out.println("					-eh / encrypted & hex");
			System.out.println("			[Text] : target text");
		} else {
			Cipher.Initialize();
			Cipher cipher = null;
			String inputTxt = args[1];
			try {
				if ("-db".equals(args[0])) {
					cipher = new Cipher(true);
					System.out.println(cipher.decrypt(inputTxt));
				} else if ("-eb".equals(args[0])) {
					cipher = new Cipher(true);
					System.out.println(cipher.encrypt(inputTxt));
				} else if ("-dh".equals(args[0])) {
					cipher = new Cipher(false);
					System.out.println(cipher.decrypt(inputTxt));
				} else if ("-eh".equals(args[0])) {
					cipher = new Cipher(false);
					System.out.println(cipher.encrypt(inputTxt));
				}
			} catch (GeneralSecurityException e) {
				e.printStackTrace();
			}
		}
	}
}
