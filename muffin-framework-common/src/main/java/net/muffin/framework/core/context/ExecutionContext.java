package net.muffin.framework.core.context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "code", "message", "response" })
@SuppressWarnings("serial")
public class ExecutionContext<T> implements Serializable {

	private final StringBuilder message = new StringBuilder();

	private String code = MuffinReturnCodeSpec.SUCC_PROCESS.code();

	@JsonProperty("response")
	private T result;

	@JsonIgnore
	private final List<MessageCode> messageCodes = new ArrayList<MessageCode>();

	private ResultStatus resultStatus;

	/**
	 * 생성자.
	 */
	public ExecutionContext() {
	}

	/**
	 * 생성자.
	 *
	 * @param messageCode
	 *            메시지코드
	 */
	public ExecutionContext(MessageCode messageCode) {
		this.messageCodes.add(messageCode);
	}

	/**
	 * 생성자.
	 *
	 * @param resultStatus
	 *            결과상태
	 * @param message
	 *            메시지
	 */
	public ExecutionContext(ResultStatus resultStatus, String message) {
		this.resultStatus = resultStatus;
		this.message.append(message);
	}

	/**
	 * 생성자.
	 *
	 * @param resultStatus
	 *            결과상태
	 * @param message
	 *            메시지
	 */
	public ExecutionContext(ResultStatus resultStatus, String code, String message) {
		this.resultStatus = resultStatus;
		this.code = code;
		this.message.append(message);
	}

	/**
	 * 결과 상태.
	 *
	 * @return ResultStatus
	 */
	public ResultStatus getResultStatus() {
		return this.resultStatus;
	}

	/**
	 * 결과 상태 설정.
	 *
	 * @param resultStatus
	 *            결과 상태
	 * @return ExecutionContext
	 */
	public ExecutionContext<T> setResultStatus(ResultStatus resultStatus) {
		this.resultStatus = resultStatus;
		return this;
	}

	/**
	 * 메시지.
	 *
	 * @return String message
	 */
	public String getMessage() {
		return this.message.toString();
	}

	/**
	 * 메시지 추가.
	 *
	 * @param message
	 *            메시지
	 * @return ExecutionContext
	 */
	public ExecutionContext<T> addMessage(String message) {
		this.message.append(message);

		return this;
	}

	/**
	 * 결과 설정.
	 *
	 * @param result
	 *            result
	 * @return ExecutionContext
	 */
	public ExecutionContext<T> setResult(T result) {
		this.result = result;

		return this;
	}

	/**
	 * 결과.
	 *
	 * @return Object
	 */
	public T getResult() {
		return this.result;
	}

	/**
	 * 메시지 코드.
	 *
	 * @return List<MessageCode>
	 */
	public List<MessageCode> getMessageCodes() {
		return this.messageCodes;
	}

	/**
	 * 메시지 코드 추가.
	 *
	 * @param messageCode
	 *            메시지코드
	 */
	public void addMessageCodes(MessageCode messageCode) {
		this.messageCodes.add(messageCode);
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
}
