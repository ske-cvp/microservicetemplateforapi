package net.muffin.framework.core.util;

import javax.servlet.http.HttpServletRequest;

public class HttpUtil {

	/**
	 * HTTP POST 방식으로 request
	 * @param String url
	 * @param HttpParameters httpParams
	 * @return
	 */
	public static String sendPostUrl(String url) {

		String params[] = url.split("\\?");
		HttpParameters httpParams = new HttpParameters();
		httpParams.addParams(params.length > 1 ? params[1] : null);

		return sendUrl(params[0], false, httpParams);
	}

	/**
	 * HTTP GET 방식으로 request
	 * @param String url
	 * @param HttpParameters httpParams
	 * @return
	 */
	public static String sendGetUrl(String url) {

		return sendUrl(url, true, null);
	}

	/**
	 * HTTP POST/GET 방식으로 request
	 * @param String url
	 * @param boolean isGetMethod
	 * @param HttpParameters httpParams
	 * @return
	 */
	public static String sendUrl(String url, boolean isGetMethod, HttpParameters httpParams) {

		return new HttpRequest().call(url, isGetMethod, httpParams);
	}

	/**
	 *
	 * <pre>AJAX요청 여부 리턴.</pre>
	 * @param request
	 * @return
	 */
	public static boolean isAjaxRequest(HttpServletRequest request) {
		return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
	}
}
