package net.muffin.framework.core.log.omc;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * <p>외부 연동 감시를 위한 OMC로그 정보 모델 클래스.</p>
 *
 * <ul>
 * <li>Created on : 2017. 7. 24.</li>
 * <li>Created by : sptek</li>
 * </ul>
 */
public class OmcContext {

	private static final long DURATION_INIT_VALUE = -1L;

	private static final String DATE_PATTERN = "yyyyMMddHHmmss";

	// 호스트명
	private String hostNm;
	// 연동 시스템
	private String remoteSystemNm;
	// 처리결과
	private OperationResult opResult;
	// 연동결과코드
	private String resultCode;
	// 처리 시간
	private long duration = DURATION_INIT_VALUE;
	// 상세 서비스명 (기능명-전문번호)
	private String serviceNm;
	// 요청 시간
	private Date requestTime;
	// 응답 시간
	private Date responseTime;
	// 연동 키1
	private String remotePrimaryKey;
	// 연동 키2
	private String remoteSecondaryKey;
	// 연동 URL
	private String remoteURL;
	// 기타 부가 정보
	private String[] extra;

	/**
	 * <pre>
	 * Contructor
	 * </pre>
	 *
	 * @param remoteSystemNm
	 *            - 연동 시스템 명칭
	 * @param requestTime
	 *            - 요청 시간
	 */
	public OmcContext(String remoteSystemNm, Date requestTime) {
		this(remoteSystemNm, EMPTY, requestTime, EMPTY, EMPTY, EMPTY);
	}

	/**
	 * <pre>
	 * Contructor
	 * </pre>
	 *
	 * @param remoteSystemNm
	 *            - 연동 시스템 명칭
	 * @param serviceNm
	 *            - 상세 서비스명 (기능명-전문번호)
	 * @param requestTime
	 *            - 요청 시간
	 * @param remotePrimaryKey
	 *            - 연동Key1
	 * @param remoteSecondaryKey
	 *            - 연동Key2
	 * @param remoteURL
	 *            - 연동URL
	 */
	public OmcContext(String remoteSystemNm, String serviceNm, Date requestTime, String remotePrimaryKey,
			String remoteSecondaryKey, String remoteURL) {
		this(EMPTY, remoteSystemNm, serviceNm, requestTime, remotePrimaryKey, remoteSecondaryKey, remoteURL);
		try {
			this.hostNm = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			// Ignore Exception
		}
	}

	/**
	 * <pre>
	 * Contructor
	 * </pre>
	 *
	 * @param hostName
	 *            - 호스트 명칭
	 * @param remoteSystemNm
	 *            - 연동 시스템 명칭
	 * @param serviceNm
	 *            - 상세 서비스명 (기능명-전문번호)
	 * @param requestTime
	 *            - 요청 시간
	 * @param remotePrimaryKey
	 *            - 연동Key1
	 * @param remoteSecondaryKey
	 *            - 연동Key2
	 * @param remoteURL
	 *            - 연동URL
	 */
	public OmcContext(String hostName, String remoteSystemNm, String serviceNm, Date requestTime,
			String remotePrimaryKey, String remoteSecondaryKey, String remoteURL) {
		this.hostNm = hostName;
		this.remoteSystemNm = (isNotEmpty(remoteSystemNm) ? remoteSystemNm : EMPTY);
		this.serviceNm = (isNotEmpty(serviceNm) ? serviceNm : EMPTY);
		this.remotePrimaryKey = (isNotEmpty(remotePrimaryKey) ? remotePrimaryKey : EMPTY);
		this.remoteSecondaryKey = (isNotEmpty(remoteSecondaryKey) ? remoteSecondaryKey : EMPTY);
		this.remoteURL = (isNotEmpty(remoteURL) ? remoteURL : EMPTY);
		this.requestTime = (requestTime != null ? requestTime : new Date());
	}

	/**
	 * <pre>
	 * 연동 결과에 대한 정보를 설정 한다.
	 * </pre>
	 *
	 * @param opResult
	 *            - 처리결과
	 * @param resultCode
	 *            - 연동 결과 코드
	 * @param responseTime
	 *            - 응답 시간
	 * @param extra
	 *            - 부가 정보
	 */
	public OmcContext finalize(OperationResult opResult, Date responseTime) {
		return this.finalize(opResult, EMPTY, responseTime, new String[] { EMPTY });
	}

	/**
	 * <pre>
	 * 연동 결과에 대한 정보를 설정 한다.
	 * </pre>
	 *
	 * @param opResult
	 *            - 처리결과
	 * @param resultCode
	 *            - 연동 결과 코드
	 * @param responseTime
	 *            - 응답 시간
	 * @param extra
	 *            - 부가 정보
	 */
	public OmcContext finalize(OperationResult opResult, String resultCode, Date responseTime, String[] extra) {
		this.opResult = (opResult != null ? opResult : OperationResult.FAIL);
		this.resultCode = (isNotEmpty(resultCode) ? resultCode : EMPTY);
		this.extra = (extra != null ? extra : new String[] {});
		this.responseTime = (responseTime != null ? responseTime : new Date());
		if (this.responseTime != null && this.requestTime != null && this.duration == DURATION_INIT_VALUE) {
			this.duration = responseTime.getTime() - this.requestTime.getTime();
		}
		return this;
	}

	/**
	 * @return the hostNm
	 */
	public String getHostNm() {
		return this.hostNm;
	}

	/**
	 * @return the remoteSystemNm
	 */
	public String getRemoteSystemNm() {
		return this.remoteSystemNm;
	}

	/**
	 * @return the opResult
	 */
	public OperationResult getOpResult() {
		return this.opResult;
	}

	/**
	 * @return the resultCode
	 */
	public String getResultCode() {
		return this.resultCode;
	}

	/**
	 * @return the duration
	 */
	public long getDuration() {
		return this.duration;
	}

	/**
	 * @return the serviceNm
	 */
	public String getServiceNm() {
		return this.serviceNm;
	}

	/**
	 * @return the requestTime
	 */
	public Date getRequestTime() {
		return this.requestTime;
	}

	/**
	 * @return the responseTime
	 */
	public Date getResponseTime() {
		return this.responseTime;
	}

	/**
	 * @return the remotePrimaryKey
	 */
	public String getRemotePrimaryKey() {
		return this.remotePrimaryKey;
	}

	/**
	 * @return the remoteSecondaryKey
	 */
	public String getRemoteSecondaryKey() {
		return this.remoteSecondaryKey;
	}

	/**
	 * @return the remoteURL
	 */
	public String getRemoteURL() {
		return this.remoteURL;
	}

	/**
	 * @return the extra
	 */
	public String[] getExtra() {
		return this.extra;
	}

	/**
	 * @return the extra using delimeter ','
	 */
	public String getExtraAsString() {
		if (this.extra == null) {
			return EMPTY;
		}
		String extraAsList = Arrays.asList(this.extra).toString();
		return extraAsList.substring(1, extraAsList.length() - 1);
	}

	/**
	 * @return the requestTime formatted "yyMMddHHmmss"
	 */
	public String getRequestTimeAsString() {
		return (new SimpleDateFormat(DATE_PATTERN)).format(this.requestTime);
	}

	/**
	 * @return the responseTime formatted "yyMMddHHmmss"
	 */
	public String getResponseTimeAsString() {
		return (new SimpleDateFormat(DATE_PATTERN)).format(this.responseTime);
	}

	/**
	 * <pre>
	 * 설정된 외부 연동 감시 모델 정보 검사하여 기록 가능 여부를 반환
	 * </pre>
	 *
	 * @return 로그 기록 가능 여부
	 */
	public boolean isLoggable() {
		if (isEmpty(this.remoteSystemNm)) {
			return false;
		}
		if (this.opResult == null) {
			return false;
		}
		if (this.requestTime == null) {
			return false;
		}
		if (this.responseTime == null) {
			return false;
		}
		if (this.duration == -1L) {
			return false;
		}
		return true;
	}

	/**
	 * <p>연동 결과 코드에 대한 Enumeration 타입</p>
	 *
	 * <ul>
	 * <li>Created on : 2017. 7. 24.</li>
	 * <li>Created by : sptek</li>
	 * </ul>
	 */
	public enum OperationResult {

		SUCCESS("S"),
		FAIL("F");

		private String code;

		private OperationResult(String code) {
			this.code = code;
		}

		/**
		 * @return the code
		 */
		public String code() {
			return this.code;
		}
	}
}
