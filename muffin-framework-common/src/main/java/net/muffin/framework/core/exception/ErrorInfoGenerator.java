package net.muffin.framework.core.exception;

import net.muffin.framework.core.context.MuffinReturnCodeSpec;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.dao.DataAccessException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;

import net.muffin.framework.core.exception.domain.ErrorInfo;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

public class ErrorInfoGenerator {

	private List<MessageSourceAccessor> messageSourceAccessorList;

	private String errorCodePrefix;

	private List<MuffinErrorCodeTranslator> muffinErrorCodeTranslatorList;

	public void setKtnovelErrorCodeTranslatorList(List<MuffinErrorCodeTranslator> muffinErrorCodeTranslatorList) {
		this.muffinErrorCodeTranslatorList = muffinErrorCodeTranslatorList;
	}

	/**
	 * <pre>
	 * 에러코드 접두어.
	 * </pre>
	 *
	 * @param errorCodePrefix
	 *            에러코드 접두어
	 */
	public void setErrorCodePrefix(String errorCodePrefix) {
		this.errorCodePrefix = errorCodePrefix;
	}

	/**
	 * <pre>
	 * 메시지소스액세서 설정 메소드.
	 * </pre>
	 *
	 * @param messageSourceAccessorList
	 *            메시지소스액세서 목록
	 */
	public void setMessageSourceAccessorList(List<MessageSourceAccessor> messageSourceAccessorList) {
		this.messageSourceAccessorList = messageSourceAccessorList;
	}

	/**  */
	/**
	 * <pre>
	 * package명으로 에러 코드 반환.
	 * </pre>
	 *
	 * @param packageName
	 *            - 패키지 명
	 * @return 에러코드
	 */
	public String getErrorCodeByPackageName(String packageName) {
		return StringUtils.defaultIfBlank(this.getSysteErrorCode(packageName), this.errorCodePrefix);
	}

	/**
	 *
	 * <pre>
	 * stactTrace에서 에러 코드 추출.
	 * </pre>
	 *
	 * @param stackTraceElements
	 *            - 예외 정보 stacktrace 객체 배열
	 * @return 에로코드
	 */
	public String getErrorCodeByStackTraceElements(StackTraceElement[] stackTraceElements) {
		String result = null;

		for (StackTraceElement stackTraceElement : stackTraceElements) {
			result = this.getSysteErrorCode(stackTraceElement.getClassName());

			if (result != null) {
				return result;
			}
		}

		return this.errorCodePrefix;
	}

	/**
	 * <pre>
	 * 에러정보 클래스의 계층구조 생성 및 예외메시지 바인딩 메소드.
	 * </pre>
	 *
	 * @param ex
	 *            예외객체
	 * @param locale
	 *            로케일
	 * @return 에러정보
	 */
	public ErrorInfo createHierarchicalErrorInfoAndSetMessage(Throwable ex, Locale locale) {

		List<Throwable> throwableList = ExceptionUtils.getThrowableList(ex);

		ErrorInfo topErrorInfo = null;

		ErrorInfo beforeErrorInfo = null;

		boolean isBreak = false;

		for (int index = 0, max = throwableList.size() > 2 ? 2 : throwableList.size(); index < max; index++) {

			Throwable throwable = throwableList.get(index);

			ErrorInfo errorInfo = null;

			if (throwable instanceof HttpMessageNotReadableException) {
				errorInfo = new ErrorInfo();
				errorInfo.setCode(MuffinReturnCodeSpec.ERR_REQ_INVALID_PARAM.code());
				errorInfo.setArgs(new Object[] { "Json Mapping Error", throwable.getMessage() });
				isBreak = true;
			} else if (throwable instanceof BindException) {
				// Hibernate 이용 POJO 관련 Validation 오류가 발생한 경우의 예외 처리
				errorInfo = this.resolveErrorInfo((BindException) throwable, locale);
			} else if (throwable instanceof MuffinXssInvalidException) {
				// Xss 공격이라고 판단되는 요청에 대한 예외 처리
				errorInfo = this.resolveErrorInfo((MuffinXssInvalidException) throwable, locale);
			} else if (throwable instanceof DataAccessException || throwable instanceof SQLException) {
				// DB 처리 관련 발생된 예외 상황에 대한 처리.
				MuffinDataAccessException daex = new MuffinDataAccessException(throwable);
				errorInfo = this.resolveErrorInfo(daex, locale);
				isBreak = true;
			} else if (MuffinNetworkException.isNetworkException(throwable)) {
				String exceptionType = throwable.getClass().getSimpleName();
				// 네트워크 관련 발생된 예외 상황에 대한 처리
				MuffinNetworkException nex = null;
				if (throwable instanceof MuffinNetworkException) {
					nex = (MuffinNetworkException) throwable;
				} else {
					nex = new MuffinNetworkException(throwable);
				}
				errorInfo = this.resolveErrorInfo(nex, locale);
				errorInfo.setArgs(new Object[] { exceptionType });
				errorInfo.setMessage(this.resolveMessage(errorInfo, locale));
				isBreak = true;
			} else if (throwable instanceof MuffinException) {
				errorInfo = ((MuffinException) throwable).getErrorInfo();
			} else {
				errorInfo = new ErrorInfo();
				errorInfo.setCode(MuffinReturnCodeSpec.ERR_SYS_GENERAL.code());
				errorInfo.setArgs(new Object[] { throwable.getClass().getName() });
			}

			if (StringUtils.isBlank(errorInfo.getMessage())) {
				errorInfo.setMessage(StringUtils.defaultIfBlank(this.resolveMessage(errorInfo, locale),
						"Not found a error message."));
			}

			// rest 통신 규격서 통일화를 위하여 hostname 적용 삭제
			// try {
			// errorInfo.setHostName(java.net.InetAddress.getLocalHost().getHostName());
			//
			// } catch (UnknownHostException e) {
			// errorInfo.setHostName("UnknownHost");
			// }

//			errorInfo.setInstanceName(System.getProperty("env.servername"));

			if (index == 0) {
				topErrorInfo = errorInfo;
			}

			if (beforeErrorInfo != null) {
				beforeErrorInfo.setCause(errorInfo);
			}

			beforeErrorInfo = errorInfo;

			if (isBreak) {
				break;
			}
		}

		return topErrorInfo;
	}

	/*
	 * <pre> package명 기반으로 에러코드 반환. </pre>
	 *
	 * @param packageName package명
	 *
	 * @return 에러코드
	 */
	private String getSysteErrorCode(String packageName) {
		String result = null;

		if (CollectionUtils.isEmpty(this.muffinErrorCodeTranslatorList)) {
			return this.errorCodePrefix;

		} else {
			for (MuffinErrorCodeTranslator MuffinErrorCodeTranslator : this.muffinErrorCodeTranslatorList) {

				result = MuffinErrorCodeTranslator.translate(packageName);

				if (StringUtils.isNotBlank(result)) {
					return result;
				}

			}
		}

		return result;
	}

	/*
	 * <pre> 에러코드에 해당하는 메시지를 취득하는 메소드. </pre>
	 *
	 * @param errorInfo 에레정보
	 *
	 * @param locale 로케일
	 *
	 * @return 에러메시지
	 */
	private String resolveMessage(ErrorInfo errorInfo, Locale locale) {
		if (CollectionUtils.isNotEmpty(this.messageSourceAccessorList)) {
			for (MessageSourceAccessor accessor : this.messageSourceAccessorList) {
				try {
					String errorCode = errorInfo.getCode();

					if (StringUtils.contains(errorCode, "SYS_ERROR")) {
						errorCode = "SYS_ERROR" + StringUtils.substringAfter(errorCode, "SYS_ERROR");
					}

					return accessor.getMessage(errorCode, errorInfo.getArgs(), locale);
				} catch (NoSuchMessageException ex) {
					// ignore
					continue;
				}
			}
		}

		return StringUtils.EMPTY;
	}

	/*
	 *
	 * <pre>DB관련 예외상황을 ErrorInfo 객체로 가공</pre>
	 *
	 * @param ex - 예외 정보
	 *
	 * @param locale - Http Request locale 정보
	 *
	 * @return ErrorInfo
	 */
	private ErrorInfo resolveErrorInfo(MuffinDataAccessException ex, Locale locale) {

		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setCode(MuffinReturnCodeSpec.ERR_SYS_DB.code());
		errorInfo.setMessage(this.resolveMessage(errorInfo, locale));

		return errorInfo;
	}

	/*
	 *
	 * <pre>네트워크 관련 예외상황을 ErrorInfo 객체로 가공</pre>
	 *
	 * @param ex - 예외 정보
	 *
	 * @param locale - Http Request locale 정보
	 *
	 * @return ErrorInfo
	 */
	private ErrorInfo resolveErrorInfo(MuffinNetworkException ex, Locale locale) {
		ErrorInfo errorInfo = new ErrorInfo();
		ErrorInfo cause = ex.getErrorInfo();

		// Cause 관련 예외 정보 존재 시 메세지 설정 처리
		if (cause != null) {
			// if (StringUtils.isEmpty(cause.getMessage())) {
			// cause.setMessage(this.resolveMessage(cause, locale));
			// }
			// errorInfo.setCause(cause);
			errorInfo = cause;
		}

		if (StringUtils.contains(ex.getMessage(), "Connection refused: connect")) {
			// java.net.ConnectException: Connection refused: connect
			errorInfo.setCode(MuffinReturnCodeSpec.ERR_NET_REQ_REJECT.code());
		} else if (StringUtils.contains(ex.getMessage(), "connect timed out")) {
			// java.net.SocketTimeoutException: connect timed out
			errorInfo.setCode(MuffinReturnCodeSpec.ERR_NET_CONN_TIMEOUT.code());
		} else if (StringUtils.contains(ex.getMessage(), "Read timed out")) {
			// java.net.SocketTimeoutException: Read timed out
			errorInfo.setCode(MuffinReturnCodeSpec.ERR_NET_SOCK_TIMEOUT.code());
		} else {
			errorInfo.setCode(MuffinReturnCodeSpec.ERR_NET_GENERAL.code());
			errorInfo.setArgs(new Object[] { ex.getClass().getName() });
		}

		// 최종 전달할 응답 정보에 대한 에러 코드 및 메세지 정보 설정.
		// errorInfo.setCode(errorCode);
		errorInfo.setMessage(this.resolveMessage(errorInfo, locale));

		return errorInfo;

	}

	/*
	 *
	 * <pre>Xss 공격으로 발생된 예외상황을 ErrorInfo 객체로 가공</pre>
	 *
	 * @param ex - 예외 정보
	 *
	 * @param locale - Http Request locale 정보
	 *
	 * @return ErrorInfo
	 */
	private ErrorInfo resolveErrorInfo(MuffinXssInvalidException ex, Locale locale) {
		ErrorInfo errorInfo = new ErrorInfo();
		String errorCode = null;

		errorCode = MuffinReturnCodeSpec.ERR_SEC_XSS.code();

		errorInfo = ex.getErrorInfo();

		// 최종 전달할 응답 정보에 대한 에러 코드 및 메세지 정보 설정.
		errorInfo.setCode(errorCode);
		errorInfo.setMessage(this.resolveMessage(errorInfo, locale));

		return errorInfo;
	}

	/*
	 *
	 * <pre> Hibernate 이용 validation 예외상황 ErrorInfo 가공. </pre>
	 *
	 * @param ex - 예외 정보
	 *
	 * @param locale - Http Request locale 정보
	 *
	 * @return ErrorInfo
	 */
	private ErrorInfo resolveErrorInfo(BindException ex, Locale locale) {
		ErrorInfo errorInfo = new ErrorInfo();
		String errorCode = null;

		String defErrCode = ex.getBindingResult().getFieldError().getCode();
		if (defErrCode.contains(NotEmpty.class.getSimpleName())) {
			errorCode = MuffinReturnCodeSpec.ERR_REQ_NO_PARAM.code();
		} else {
			errorCode = MuffinReturnCodeSpec.ERR_REQ_INVALID_PARAM.code();
		}

		Object[] args = new Object[] { ex.getBindingResult().getFieldError().getField(),
				ex.getBindingResult().getFieldError().getRejectedValue() };

		// 최종 전달할 응답 정보에 대한 에러 코드 및 메세지 정보 설정.
		errorInfo.setCode(errorCode);
		errorInfo.setArgs(args);
		errorInfo.setMessage(this.resolveMessage(errorInfo, locale));

		return errorInfo;
	}
}
