package net.muffin.framework.core.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.FileCopyUtils;

public class FileUtils {

	protected static Log logger = LogFactory.getLog(FileUtils.class);

	/**
	 * 파일명을 전달받아 확장자를 반환한다.
	 *
	 * @param String
	 *            filename : 파일명
	 * @return String extension : 확장자
	 */
	public static String getExtension(String filename) {

		String extension = "";

		if (filename != null) {
			int lm_iIndex = filename.lastIndexOf('.');

			if (lm_iIndex > -1) {
				extension = filename.substring(lm_iIndex + 1);
			}
		}

		return extension;

	}

	/**
	 * 현재시간 기준으로 파일을 저장할 /YYYY/MM/DD 형태의 경로를 가져온다.
	 *
	 * @return String datePath : /YYYY/MM/DD 형태의 경로
	 */
	public static String getDatePath() {

		String datePath = ""; // 반환할 경로

		// 현재 날짜를 세팅한다.
		Date date = new Date();
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(date);

		String year = Integer.toString(gregorianCalendar.get(Calendar.YEAR)); // 년
		String month = Integer.toString(gregorianCalendar.get(Calendar.MONTH) + 1); // 월
		String day = Integer.toString(gregorianCalendar.get(Calendar.DATE)); // 일

		// 월일시분초가 1자리로 나올 경우 2자리로 맞춘다.
		if (month.length() == 1) {
			month = "0" + month;
		}

		if (day.length() == 1) {
			day = "0" + day;
		}

		datePath = "/" + year + "/" + month + "/" + day;

		return datePath;

	}

	/**
	 * 확장자를 전달받아 업로드 가능한 파일인지 여부를 반환한다.
	 *
	 * @param String
	 *            fileExt : 확장자
	 * @return boolean : 업로드 가능한 파일인지 여부
	 */
	public static boolean isAllowedFile(String fileExt) {
		return isAllowedFile(fileExt, "");
	}

	/**
	 * 확장자와 허용 확장자를 전달받아 업로드 가능한 파일인지 여부를 반환한다.
	 *
	 * @param String
	 *            fileExt : 확장자
	 * @return boolean : 업로드 가능한 파일인지 여부
	 */
	public static boolean isAllowedFile(String fileExt, String extList) {

		boolean isAllowed = true;

		if (fileExt == null) {
			return false;
		}

		if (fileExt.length() == 0) {
			return true;
		}

		String lowerFileExt = fileExt.toLowerCase().trim();

		if ("".equals(extList)) {
			extList = "cab, exe, asp, jsp, aspx, php, cgi, js, eml, wap, ocp, dll";
		}

		if (extList.indexOf(lowerFileExt) != -1) {
			isAllowed = false;
		}

		return isAllowed;

	}

	/**
	 * 확장자와 허용 확장자를 전달받아 업로드 가능한 파일인지 여부를 반환한다.
	 *
	 * @param String
	 *            fileExt : 확장자
	 * @return boolean : 업로드 가능한 파일인지 여부
	 */
	public static boolean isAllowedTermsFile(String fileExt) {

		boolean isAllowed = true;

		if (fileExt == null) {
			return false;
		}

		if (fileExt.length() == 0) {
			return true;
		}

		String lowerFileExt = fileExt.toLowerCase().trim();

		String extList = "pdf";

		if (extList.indexOf(lowerFileExt) == -1) {
			isAllowed = false;
		}

		return isAllowed;

	}

	/**
	 * 확장자를 전달받아 이미지 파일인지 여부를 반환한다.
	 * @see #isValidImage 함께 사용 권장
	 *
	 * @param String
	 *            fileExt : 확장자
	 * @return boolean : 이미지 파일인지 여부
	 */
	public static boolean isImageFile(String fileExt) {

		boolean isImage = false;

		if (fileExt == null) {
			return false;
		}

		if (fileExt.length() == 0) {
			return false;
		}

		String lowerFileExt = fileExt.toLowerCase().trim();

		String extList = "jpg, jpeg, png";

		if (extList.indexOf(lowerFileExt) > -1) {
			isImage = true;
		}

		return isImage;

	}


    /**
     *
     * <pre>정상적인 image file 여부 리턴.</pre>
     * @param multipartFile
     * @return
     */
    /*public static boolean isValidImage(MultipartFile multipartFile) {
    	String detectedType = "";
		try {
			Tika tika = new Tika();
			detectedType = tika.detect(multipartFile.getBytes());
		} catch (IOException e) {
			logger.error("Exception Occurred while checking image files", e);
			return false;
		}
		return detectedType.contains("image/");
    }*/

	/**
	 * 원본 파일풀경로와 이동될 파일풀경로를 전달받아 해당 파일을 이동시킨다.
	 *
	 * @param String
	 *            src : 원본 파일풀경로
	 * @param String
	 *            des : 이동될 파일풀경로
	 * @return boolean isSuccess : 결과
	 */
	public boolean move(String src, String des) {

		boolean isSuccess = false; // 복사 성공여부
		int bufferSize = 32768; // Buffer Size
		BufferedInputStream bufferedInputStream = null;
		BufferedOutputStream bufferedOutputStream = null;
		FileInputStream fis = null;
		FileOutputStream fos = null;
		File file = new File(src);

		try {

			if (!file.exists()) {
				return isSuccess;
			}

			File saveFolder = new File(des.substring(0, des.lastIndexOf("/")));

			logger.debug(">>>>>>>> saveFolder : : " + saveFolder.getPath());
			logger.debug(">>>>>>>> saveFolder ab : : " + saveFolder.getAbsolutePath());
			if (!saveFolder.exists()) {
				saveFolder.mkdirs();
			}
			fis = new FileInputStream(src);
			fos = new FileOutputStream(des);
			bufferedInputStream = new BufferedInputStream(fis);
			bufferedOutputStream = new BufferedOutputStream(fos);

			byte[] buffer = new byte[bufferSize];

			int i = 0;

			while ((i = bufferedInputStream.read(buffer)) != -1) {
				bufferedOutputStream.write(buffer, 0, i);
				bufferedOutputStream.flush();
			}

			// 복사가 끝나면 원본을 삭제한다.
			if (file.exists()) {
				file.delete();
			}

			isSuccess = true;

		} catch (Exception e) {
			logger.error(e);
		} finally {
			if (fis != null)
				try {
					fis.close();
				} catch (Exception e) {
				}
			if (fos != null)
				try {
					fos.close();
				} catch (Exception e) {
				}
			if (bufferedInputStream != null)
				try {
					bufferedInputStream.close();
				} catch (Exception e) {
				}
			if (bufferedOutputStream != null)
				try {
					bufferedOutputStream.close();
				} catch (Exception e) {
				}
		}

		if (isSuccess && file.exists()) {
			file.delete();
		}

		return isSuccess;

	}

	/**
	 * 원본 파일풀경로와 이동될 파일풀경로를 전달받아 해당 파일을 복사 시킨다.
	 *
	 * @param String
	 *            src : 원본 파일풀경로
	 * @param String
	 *            des : 이동될 파일풀경로
	 * @return boolean isSuccess : 결과
	 */
	public boolean copy(String src, String des) {

		boolean isSuccess = false; // 복사 성공여부
		int bufferSize = 32768; // Buffer Size
		BufferedInputStream bufferedInputStream = null;
		BufferedOutputStream bufferedOutputStream = null;
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {

			File file = new File(src);

			if (!file.exists()) {
				return isSuccess;
			}

			File saveFolder = new File(des.substring(0, des.lastIndexOf("/")));

			if (!saveFolder.exists()) {
				saveFolder.mkdirs();
			}
			fis = new FileInputStream(src);
			fos = new FileOutputStream(des);
			bufferedInputStream = new BufferedInputStream(fis);
			bufferedOutputStream = new BufferedOutputStream(fos);

			byte[] buffer = new byte[bufferSize];

			int i = 0;

			while ((i = bufferedInputStream.read(buffer)) != -1) {
				bufferedOutputStream.write(buffer, 0, i);
				bufferedOutputStream.flush();
			}

			isSuccess = true;

		} catch (Exception e) {
			logger.error(e);
		} finally {
			if (fis != null)
				try {
					fis.close();
				} catch (Exception e) {
				}
			if (fos != null)
				try {
					fos.close();
				} catch (Exception e) {
				}
			if (bufferedInputStream != null)
				try {
					bufferedInputStream.close();
				} catch (Exception e) {
				}
			if (bufferedOutputStream != null)
				try {
					bufferedOutputStream.close();
				} catch (Exception e) {
				}
		}

		return isSuccess;

	}

	/**
	 * 파일을 삭제한다.
	 *
	 * @param String
	 *            src : 원본 파일풀경로
	 * @return boolean isSuccess : 결과
	 */
	public boolean delete(String src) {

		boolean isSuccess = false; // 삭제 성공여부

		try {

			File file = new File(src);

			if (file.exists()) {
				file.delete();
			} else {
				return isSuccess;
			}

			isSuccess = true;

		} catch (Exception e) {
			logger.error(e);
		}

		return isSuccess;

	}

	/**
	 * txt 파일경로를 전달받아 파일을 읽어 내용을 리턴하는 메서드
	 *
	 * @param String
	 *            filepath : 파일경로
	 * @return String : 파일 내용
	 */
	public static String readTextFile(String filepath) {

		String temp = "";
		String result = "";
		Charset charset = null;
		BufferedReader br = null;
		InputStreamReader isr = null;
		FileInputStream fis = null;
		File file = new File(filepath);

		if (!file.exists()) {
			logger.debug("[readTextFile] file not exist");
			return "";
		}

		charset = detectCharset(file);

		if (charset != null) {

			logger.debug("[readTextFile] filepath:" + filepath);

			try {
				fis = new FileInputStream(file);
				isr = new InputStreamReader(fis, charset);
				br = new BufferedReader(isr);

				while ((temp = br.readLine()) != null) {
					result += temp + "\n";
				}

				// ANSI 분기처리 - 20150212
				if ("windows-1252".equals(charset.toString()) || "EUC-KR".equals(charset.toString())) {
					result = new String(result.getBytes(charset), "KSC5601");
				}

				logger.debug("[readTextFile] charset:" + charset.toString());
				logger.debug("[readTextFile] read text:" + result);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (fis != null)
					try {
						fis.close();
					} catch (Exception e) {
					}
				if (isr != null)
					try {
						isr.close();
					} catch (Exception e) {
					}
				if (br != null)
					try {
						br.close();
					} catch (Exception e) {
					}
			}

		} else {
			logger.debug("[readTextFile] Unrecognized charset");
		}

		return result;
	}

	/**
	 * txt 파일의 charset을 가져온다.
	 *
	 * @param File
	 *            f : 파일
	 * @return Charset : charset
	 */
	// public static final String[] ENCODINGS = new String[] {"windows-1252", "EUC-KR", "UTF-8", "UTF-16LE", "UTF-16BE",
	// "Cp1252"};
	public static final String[] ENCODINGS = new String[] { "EUC-KR", "UTF-8", "UTF-16LE", "UTF-16BE", "Cp1252" };

	public static Charset detectCharset(File f) {

		Charset charset = null;

		for (String charsetName : ENCODINGS) {
			charset = detectCharset(f, Charset.forName(charsetName));
			if (charset != null) {
				break;
			}
		}

		return charset;
	}

	public static Charset detectCharset(File f, Charset charset) {

		try {

			BufferedInputStream input = new BufferedInputStream(new FileInputStream(f));

			CharsetDecoder decoder = charset.newDecoder();
			decoder.reset();

			byte[] buffer = new byte[512];
			boolean identified = false;
			while ((input.read(buffer) != -1) && (!identified)) {
				identified = identify(buffer, decoder);
			}

			input.close();

			if (identified) {
				return charset;
			} else {
				return null;
			}

		} catch (Exception e) {
			return null;
		}
	}

	public static boolean identify(byte[] bytes, CharsetDecoder decoder) {
		try {
			decoder.decode(ByteBuffer.wrap(bytes));
		} catch (CharacterCodingException e) {
			return false;
		}
		return true;
	}

	/**
	 * 파일을 다운로드 한다.
	 *
	 * @param request
	 *            request
	 * @param HttpServletResponse
	 *            response
	 * @return void
	 */
	public static boolean fileDownload(HttpServletRequest request, HttpServletResponse response, String rootPath) {

		String filepath = request.getParameter("filepath");
		String filenm = request.getParameter("filenm");

		BufferedInputStream in = null;
		BufferedOutputStream out = null;

		try {

			File file = new File(rootPath + filepath);
			int fSize = (int) file.length();

			if (fSize > 0) {
				String mimetype = "application/x-msdownload";

				response.setContentType(mimetype);
				// response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(filenm,
				// "utf-8") + "\"");
				setDisposition(filenm, request, response);
				response.setContentLength(fSize);

				in = new BufferedInputStream(new FileInputStream(file));
				out = new BufferedOutputStream(response.getOutputStream());

				FileCopyUtils.copy(in, out);
				out.flush();

				return true;
			}

		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			try {
				if (in != null)
					in.close();
				if (out != null)
					out.close();
			} catch (Exception ignore) {
				logger.debug("[fileDownload]: " + ignore.getMessage());
			}
		}

		return false;
	}

	/**
	 * Disposition 지정하기.
	 *
	 * @param filename
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	private static void setDisposition(String filename, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String browser = getBrowser(request);

		String dispositionPrefix = "attachment; filename=";
		String encodedFilename = null;

		if (browser.equals("MSIE")) {
			encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
		} else if (browser.equals("Firefox")) {
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		} else if (browser.equals("Opera")) {
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		} else if (browser.equals("Chrome")) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < filename.length(); i++) {
				char c = filename.charAt(i);
				if (c > '~') {
					sb.append(URLEncoder.encode("" + c, "UTF-8"));
				} else {
					sb.append(c);
				}
			}
			encodedFilename = sb.toString();
		} else {
			// throw new RuntimeException("Not supported browser");
			throw new IOException("Not supported browser");
		}

		response.setHeader("Content-Disposition", dispositionPrefix + encodedFilename);

		if ("Opera".equals(browser)) {
			response.setContentType("application/octet-stream;charset=UTF-8");
		}
	}

	/**
	 * 브라우저 구분 얻기.
	 *
	 * @param request
	 * @return
	 */
	public static String getBrowser(HttpServletRequest request) {
		String header = request.getHeader("User-Agent");
		if (header.indexOf("MSIE") > -1 || header.indexOf("Trident") > -1) {
			return "MSIE";
		} else if (header.indexOf("Chrome") > -1) {
			return "Chrome";
		} else if (header.indexOf("Opera") > -1) {
			return "Opera";
		}
		return "Firefox";
	}

	public static String getCurrentTimeFileName(String originalFilename) {
		String fileExt = FileUtils.getExtension(originalFilename); // 확장자
		String fileNm = String.valueOf(System.currentTimeMillis()) + "." + fileExt;
		return fileNm;
	}

	public static File createSaveFolder(String path) {
		File saveFolder = new File(path);
		if (!saveFolder.exists()) {
			saveFolder.mkdirs();
		}

		return saveFolder;
	}

}
