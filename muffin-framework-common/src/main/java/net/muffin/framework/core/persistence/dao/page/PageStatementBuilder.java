package net.muffin.framework.core.persistence.dao.page;

public class PageStatementBuilder {

	private String totalCountStatementId;
	private String dataStatementId;

	public static PageStatementBuilder create() {
		return new PageStatementBuilder();
	}

	public PageStatementBuilder totalCountStatementId(String totalCountStatementId) {
		this.totalCountStatementId = totalCountStatementId;
		return this;
	}

	public PageStatementBuilder dataStatementId(String dataStatementId) {
		this.dataStatementId = dataStatementId;
		return this;
	}

	public PageStatement build() {
		return new PageStatement(this.totalCountStatementId, this.dataStatementId);
	}

	public static PageStatement buildStatement(final String totalCountStatementId, final String dataStatementId) {
		return new PageStatement() {

			@Override
			public String getTotalCountStatementId() {
				return totalCountStatementId;
			}

			@Override
			public String getDataStatementId() {
				return dataStatementId;
			}
		};
	}
}
