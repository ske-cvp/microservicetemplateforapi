package net.muffin.framework.core.persistence.dao.page;

public class PageStatement {

	private String totalCountStatementId;
	private String dataStatementId;

	public PageStatement(String totalCountStatementId, String dataStatementId) {
		super();
		this.totalCountStatementId = totalCountStatementId;
		this.dataStatementId = dataStatementId;
	}

	public PageStatement() {
		super();
	}

	public String getTotalCountStatementId() {
		return this.totalCountStatementId;
	}

	public String getDataStatementId() {
		return this.dataStatementId;
	}
}
