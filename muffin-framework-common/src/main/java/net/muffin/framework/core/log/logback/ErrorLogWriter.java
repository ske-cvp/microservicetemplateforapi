package net.muffin.framework.core.log.logback;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.muffin.framework.core.exception.domain.ErrorInfo;

public class ErrorLogWriter {

	private static Logger logger = LoggerFactory.getLogger(ErrorLogWriter.class);

	/**
	 * <pre>
	 * errorLogWrite.
	 * errorInfo.getCode() 의 값에 SYS_ 가 있다면(정의 되지 않은 ErrorCode) HttpStatus 500 리턴, 그외에는 HttpStatus 200 을 리턴 한다.
	 * HttpStatus 500 이 리턴 될 경우, full StackTrace 가 로깅 된다.
	 * HttpStatus 200 이 리턴 될 경우, 5개의 StackTrace 가 로깅 된다.
	 * </pre>
	 *
	 * @param errorInfo
	 *            ErrorInfo
	 * @return HttpStatus
	 */
	public static int errorLogWrite(ErrorInfo errorInfo) {
		return ErrorLogWriter.errorLogWrite(errorInfo, null);
	}

	/**
	 * <pre>
	 * errorLogWrite.
	 * errorInfo.getCode() 의 값에 SYS_ 가 있다면(정의 되지 않은 ErrorCode) HttpStatus 500 리턴, 그외에는 HttpStatus 200 을 리턴 한다.
	 * HttpStatus 500 이 리턴 될 경우, full StackTrace 가 로깅 된다.
	 * HttpStatus 200 이 리턴 될 경우, 5개의 StackTrace 가 로깅 된다.
	 * </pre>
	 *
	 * @param errorInfo
	 *            ErrorInfo
	 * @param ex
	 *            Exception
	 * @return HttpStatus
	 */
	public static int errorLogWrite(ErrorInfo errorInfo, Exception ex) {
		int httpStatus = -1;

		try {
			if (errorInfo.getCode().indexOf("SYS_") > -1) {

				if (ex == null) {

					logger.error(
							"throw unexpected exception CODE : " + errorInfo.getCode() + ", MESSAGE : " + errorInfo.getMessage());
				} else {

					StackTraceElement[] elements = ex.getStackTrace();
					int maxLen = elements.length > 5 ? 5 : elements.length;
					Object[] args = new String[maxLen + 1];

					args[0] = ex.getCause() == null ? "" : ex.getCause().toString();

					for (int index = 0; index < maxLen; index++) {
						args[index + 1] = elements[index].getClassName() + "." + elements[index].getMethodName() + "("
								+ elements[index].getFileName() + ":" + elements[index].getLineNumber() + ")";
					}
					logger.error("throw unexpected exception CODE : " + errorInfo.getCode() + ", MESSAGE : "
							+ errorInfo.getMessage() + "{}\n{}\n{}\n{}\n{}\n{}", args);
				}
				httpStatus = HttpStatus.SC_INTERNAL_SERVER_ERROR;

				/*logger.error("throw unexpected exception CODE : " + errorInfo.getCode() + ", MESSAGE : "
						+ errorInfo.getMessage(), ex);

				httpStatus = HttpStatus.SC_INTERNAL_SERVER_ERROR;*/

			} else {
				if (ex == null) {
					logger.error(
							"throw exception CODE : " + errorInfo.getCode() + ", MESSAGE : " + errorInfo.getMessage());
				} else {
					StackTraceElement[] elements = ex.getStackTrace();
					int maxLen = elements.length > 5 ? 5 : elements.length;
					Object[] args = new String[maxLen + 1];

					args[0] = ex.getCause() == null ? "" : ex.getCause().toString();

					for (int index = 0; index < maxLen; index++) {
						args[index + 1] = elements[index].getClassName() + "." + elements[index].getMethodName() + "("
								+ elements[index].getFileName() + ":" + elements[index].getLineNumber() + ")";
					}
					logger.error("throw exception CODE : " + errorInfo.getCode() + ", MESSAGE : "
							+ errorInfo.getMessage() + "\n{}\n{}\n{}\n{}\n{}\n{}", args);
				}
				httpStatus = HttpStatus.SC_OK;
			}
		} catch (Exception iex) {
			/**
			 * 로깅 과정 중 Exception 이 발생 할 경우 해당 Exception 은 무시 하고. HttpStatus 200 리턴.
			 **/
			logger.warn("errorLogWrite : {}", iex);

			logger.error("throw unexpected exception CODE : " + errorInfo.getCode() + ", MESSAGE : "
					+ errorInfo.getMessage(), ex);
			httpStatus = HttpStatus.SC_OK;
		}
		return httpStatus;
	}

}
