package net.muffin.framework.core.persistence.dao.handler;

public interface StreamHandler<T, R> {

	/**
	 * 스트리밍 헤더 쓰기.
	 */
	void open();

	/**
	 * 스트리밍 마무리.
	 */
	void close();

	/**
	 * 행 단위 처리.
	 *
	 * @param valueObject
	 *            행 데이터.
	 * @return the r
	 */
	R handleRow(T valueObject);

	/**
	 * Stop.
	 */
	void stop();

	/**
	 * Checks if is stop.
	 *
	 * @return true, if is stop
	 */
	boolean isStop();
}
