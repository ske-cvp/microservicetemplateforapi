package net.muffin.framework.core.crypto.asymmetric;

import net.muffin.framework.core.crypto.DataEncryptKeyPair;

/**
 * <p>RSA공개키로 암호화 된 AES대칭키와 해당 대칭키의 인덱스 정보를 제공</p>
 *
 * <ul>
 * <li>Created on : 2017. 6. 8.</li>
 * <li>Created by : sptek</li>
 * </ul>
 */
public class RsaEncryptKeyProvider {

	private String[] aesKeys;

	private RsaEncryptor rsaEncryptor;

	public RsaEncryptKeyProvider(RsaEncryptor rsaEncryptor, String[] aesKeys) {
		this.rsaEncryptor = rsaEncryptor;
		this.aesKeys = aesKeys;
	}

	public DataEncryptKeyPair getEncryptKeyPair(String rsaPubStr) {
		int keyIdx = (int) (Math.random() * this.aesKeys.length);
		String key = this.rsaEncryptor.rsaEncrypt(this.aesKeys[keyIdx], rsaPubStr);
		return new DataEncryptKeyPair(key, keyIdx);
	}
}
