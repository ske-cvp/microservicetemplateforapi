package net.muffin.framework.core.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpRequest {
	private static final Logger logger = LoggerFactory.getLogger(HttpRequest.class);

	@SuppressWarnings("deprecation")
	public String call(String url, boolean isGetMethod, HttpParameters httpParams) {

		HttpGet httpGet = null;
		HttpPost httpPost = null;
		HttpResponse response = null;
		HttpClient client = HttpClientBuilder.create().build();

		try {

			if (isGetMethod) {
				httpGet = new HttpGet(url);
				response = client.execute(httpGet);
			} else {
				httpPost = new HttpPost(url);
				if (httpParams != null && !httpParams.isEmpty()) {
					UrlEncodedFormEntity reqEntity = new UrlEncodedFormEntity(httpParams);
					httpPost.setEntity(reqEntity);
				}

				response = client.execute(httpPost);
			}

			if (response.getStatusLine().getStatusCode() == HttpsURLConnection.HTTP_OK) {

				BufferedReader bufferedReader = null;

				try {

					bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

					StringBuffer result = new StringBuffer();
					String line = "";
					while ((line = bufferedReader.readLine()) != null) {
						result.append(line);
					}

					return result.toString();

				} catch (Exception e) {
					logger.error(e.getMessage());
				} finally {
					if (bufferedReader != null)
						bufferedReader.close();
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if (client != null) {
				client.getConnectionManager().shutdown();
			}
		}

		return null;
	}

	@SuppressWarnings("deprecation")
	public Map<String, String> callForMapTest(String url, boolean isGetMethod, HttpParameters httpParams) {
		Map<String, String> returnMap = new HashMap<>();
		String result = "P_STATUS=00&P_AUTH_DT=20170817204139&P_AUTH_NO=30031838&P_RMESG1=성공적으로 처리 하였습니다.&P_RMESG2=00&P_TID=INIMX_ISP_INIpayTest20170817204138761219&P_FN_CD1=06&P_AMT=2700&P_TYPE=CARD&P_UNAME=mijoo619@gmail.com&P_MID=INIpayTest&P_OID=20170817204052656846&P_NOTI=&P_NEXT_URL=http://v2.mysptek.com/mw/mysptek/payment/barry_charge_result.kt&P_MNAME=&P_NOTEURL=&P_CARD_MEMBER_NUM=&P_CARD_NUM=467309*********3&P_CARD_ISSUER_CODE=04&P_CARD_PURCHASE_CODE=06&P_CARD_PRTC_CODE=1&P_CARD_INTEREST=0&P_CARD_CHECKFLAG=1&P_CARD_ISSUER_NAME=국민카드&P_CARD_PURCHASE_NAME=국민계열&P_FN_NM=국민계열&P_ISP_CARDCODE=040204040016646&P_CARD_APPLPRICE=2700";

		String[] params = result.split("&");
		for (String param : params) {
			String[] keyValues = param.split("=");
			if (keyValues.length > 1) {
				logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> key  : " + keyValues[0]);
				logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> value : " + keyValues[1]);
				returnMap.put(keyValues[0], keyValues[1]);
			}
		}

		return returnMap;
	}

	@SuppressWarnings("deprecation")
	public Map<String, String> callForMap(String url, boolean isGetMethod, HttpParameters httpParams) {

		HttpGet httpGet = null;
		HttpPost httpPost = null;
		HttpResponse response = null;
		HttpClient client = HttpClientBuilder.create().build();

		Map<String, String> returnMap = null;
		try {

			if (isGetMethod) {
				httpGet = new HttpGet(url);
				response = client.execute(httpGet);
			} else {
				httpPost = new HttpPost(url);
				if (httpParams != null && !httpParams.isEmpty()) {
					UrlEncodedFormEntity reqEntity = new UrlEncodedFormEntity(httpParams);
					httpPost.setEntity(reqEntity);
				}

				response = client.execute(httpPost);
			}

			if (response.getStatusLine().getStatusCode() == HttpsURLConnection.HTTP_OK) {

				BufferedReader bufferedReader = null;

				try {

					bufferedReader = new BufferedReader(
							new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
					StringBuffer result = new StringBuffer();
					String line = "";
					while ((line = bufferedReader.readLine()) != null) {
						result.append(line);
						logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> line : " + line);
						logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> result string : " + result.toString());
					}

					if (result != null && !"".equals(result.toString())) {
						returnMap = new HashMap<String, String>();
						String[] params = result.toString().split("&");
						for (String param : params) {
							String[] keyValues = param.split("=");
							if (keyValues.length > 1) {
								logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> key : " + keyValues[0]);
								logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> value : " + keyValues[1]);
								returnMap.put(keyValues[0], keyValues[1]);
							}
						}
					}

					return returnMap;

				} catch (Exception e) {
					logger.error(e.getMessage());
				} finally {
					if (bufferedReader != null)
						bufferedReader.close();
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if (client != null) {
				client.getConnectionManager().shutdown();
			}
		}

		return null;
	}

	@SuppressWarnings("deprecation")
	public JSONObject callForJson(String url, JSONObject jsonParam) {
		logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> callForJson url : " + url);
		logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> callForJson jsonParam : " + jsonParam.toString());
		HttpGet httpGet = null;
		HttpPost httpPost = null;
		HttpResponse response = null;
		HttpClient client = HttpClientBuilder.create().build();

		Map<String, String> returnMap = null;
		JSONArray jsonArray = null;
		JSONObject jsonObject = null;
		try {

			httpPost = new HttpPost(url);
			if (jsonParam != null) {
				// UrlEncodedFormEntity reqEntity = new UrlEncodedFormEntity(httpParams);

				String jsonString = jsonParam.toString().replaceAll("\\\\", "");
				logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> jsonString 1 : " + jsonString);
				StringEntity reqEntity = new StringEntity(jsonString);

				httpPost.setEntity(reqEntity);
			}

			httpPost.setHeader("Content-type", "application/json");
			response = client.execute(httpPost);

			if (response.getStatusLine().getStatusCode() == HttpsURLConnection.HTTP_OK) {

				BufferedReader bufferedReader = null;

				try {

					bufferedReader = new BufferedReader(
							new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
					StringBuffer result = new StringBuffer();
					String line = "";
					while ((line = bufferedReader.readLine()) != null) {
						result.append(line);
						logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> line : " + line);
						logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> result string : " + result.toString());
					}

					jsonObject = new JSONObject(result.toString());
					logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> jsonObject : " + jsonObject);

					return jsonObject;

				} catch (Exception e) {
					logger.error(e.getMessage());
				} finally {
					if (bufferedReader != null)
						bufferedReader.close();
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if (client != null) {
				client.getConnectionManager().shutdown();
			}
		}

		return null;
	}

	@SuppressWarnings("deprecation")
	public JSONObject callForJson(String url, List<NameValuePair> param) {
		logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> callForJson url : " + url);
		logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> callForJson param : " + param.toString());
		logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> callForJson param : " + param);
		HttpGet httpGet = null;
		HttpPost httpPost = null;
		HttpResponse response = null;
		HttpClient client = HttpClientBuilder.create().build();

		Map<String, String> returnMap = null;
		JSONArray jsonArray = null;
		JSONObject jsonObject = null;
		try {

			httpPost = new HttpPost(url);
			if (param != null) {
				// UrlEncodedFormEntity reqEntity = new UrlEncodedFormEntity(httpParams);
				// logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>> callForJson reqEntity : " + param.toString());
				httpPost.setEntity(new UrlEncodedFormEntity(param));

				// httpPost.setEntity(entity);
			}

			httpPost.setHeader("Content-type", "application/json");
			response = client.execute(httpPost);

			if (response.getStatusLine().getStatusCode() == HttpsURLConnection.HTTP_OK) {

				BufferedReader bufferedReader = null;

				try {

					bufferedReader = new BufferedReader(
							new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
					StringBuffer result = new StringBuffer();
					String line = "";
					while ((line = bufferedReader.readLine()) != null) {
						result.append(line);
						logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> line : " + line);
						logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> result string : " + result.toString());
					}

					jsonObject = new JSONObject(result.toString());
					logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> jsonObject : " + jsonObject);

					return jsonObject;

				} catch (Exception e) {
					logger.error(e.getMessage());
				} finally {
					if (bufferedReader != null)
						bufferedReader.close();
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if (client != null) {
				client.getConnectionManager().shutdown();
			}
		}

		return null;
	}
}
