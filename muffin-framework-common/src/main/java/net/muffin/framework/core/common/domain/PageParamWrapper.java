package net.muffin.framework.core.common.domain;

import net.muffin.framework.core.persistence.dao.page.PaginateInfo;

import lombok.Data;

@Data
public class PageParamWrapper {
	private PaginateInfo page;
	private Object condition;
}
