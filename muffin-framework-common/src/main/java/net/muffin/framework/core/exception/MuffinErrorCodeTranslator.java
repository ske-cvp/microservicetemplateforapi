package net.muffin.framework.core.exception;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public abstract class MuffinErrorCodeTranslator {

	/**
	 * 기본 package 명
	 */
	protected static final String BASE_PACKAGE_NAME = "net.muffin.";

	/**
	 * 에러코드 MAP
	 */
	protected Map<String, String> errorCodeMap;

	/**
	 *
	 * <pre>
	 * 생성자.
	 * </pre>
	 */
	protected MuffinErrorCodeTranslator() {

		this.errorCodeMap = new HashMap<String, String>();

		String frameworkPkgNm = BASE_PACKAGE_NAME + "framework";

		this.errorCodeMap.put(frameworkPkgNm, sptekFrameworkErrorCodeSpec.ERR_FRWK_COMMON.code());
		this.errorCodeMap.put(frameworkPkgNm + ".core.cryto", sptekFrameworkErrorCodeSpec.ERR_FRWK_CRYTO.code());
		this.errorCodeMap.put(frameworkPkgNm + ".core.persistence.dao",
				sptekFrameworkErrorCodeSpec.ERR_FRWK_DAO.code());
		this.errorCodeMap.put(frameworkPkgNm + ".core.log", sptekFrameworkErrorCodeSpec.ERR_FRWK_LOG.code());
		this.errorCodeMap.put(frameworkPkgNm + ".web", sptekFrameworkErrorCodeSpec.ERR_FRWK_WEB.code());
	}

	/**
	 *
	 * <pre>
	 * 최상위 package 명 반환.
	 * </pre>
	 *
	 * @param packagee
	 *            package 명
	 * @return 최상위 package 명
	 */
	public String translate(String packagee) {
		java.util.List<String> splitedPackageList = Arrays.asList(StringUtils.split(packagee, "."));

		String result;

		for (int index = splitedPackageList.size() - 1, min = 0; index >= min; index--) {
			String matchPkgNm = StringUtils.join(splitedPackageList.subList(0, index), ".");
			result = this.errorCodeMap.get(matchPkgNm);

			if (result != null) {
				return result;
			}
		}

		return null;
	}

	enum sptekFrameworkErrorCodeSpec {
		ERR_FRWK_COMMON("ERR_FRWK_0001", "ERR_FRWK_0001"), ERR_FRWK_CRYTO("ERR_CRYP_0001",
				"ERR_CRYP_0001"), ERR_FRWK_DAO("ERR_DAO_0001", "ERR_DAO_0001"), ERR_FRWK_LOG("ERR_LOG_0001",
						"ERR_LOG_0001"), ERR_FRWK_WEB("ERR_WEB_0001", "ERR_WEB_0001");

		private String code;

		private String msgCode;

		private sptekFrameworkErrorCodeSpec(String code, String msgCode) {
			this.code = code;
			this.msgCode = msgCode;
		}

		/**
		 * @return the code
		 */
		public String code() {
			return this.code;
		}

		/**
		 * @return the msgCode
		 */
		public String messageCode() {
			return this.msgCode;
		}
	}
}
