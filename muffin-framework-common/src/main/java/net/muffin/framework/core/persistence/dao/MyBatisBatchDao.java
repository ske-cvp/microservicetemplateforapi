package net.muffin.framework.core.persistence.dao;

import java.util.List;

import org.apache.ibatis.executor.BatchResult;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class MyBatisBatchDao extends MyBatisCommonDao implements BatchDao {
	@Autowired
	@Qualifier("batchSqlSessionTemplate")
	private SqlSessionTemplate batchSqlSessionTemplate;

	public MyBatisBatchDao() {
		super.template = batchSqlSessionTemplate;
	}

	public MyBatisBatchDao(SqlSessionTemplate sqlSessionTemplate) {
		super.template = sqlSessionTemplate;
	}

	@Override
	public List<BatchResult> flushStatements() {
		return template.flushStatements();
	}
}
