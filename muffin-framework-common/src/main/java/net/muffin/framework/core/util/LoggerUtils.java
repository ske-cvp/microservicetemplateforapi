package net.muffin.framework.core.util;

import java.io.File;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.rolling.DefaultTimeBasedFileNamingAndTriggeringPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;

public class LoggerUtils {

	private static final String DEFAULT_PATTERN_LAYOUT = "[%d{yyyy-MM-dd HH:mm:ss.SSS}|%-5level|%class{36}:%file:%method:%line]\t%msg%n";

	/**
	 * <pre>
	 * 주어진 파라메터에 맞는 Logger를 생성하여 반환한다.
	 * </pre>
	 *
	 * @param level
	 *            로그 레벨.
	 * @param loggerName
	 *            로거 이름.
	 * @param filePath
	 *            로그 기록 경로.
	 * @return
	 */
	public static Logger createLogger(Level level, String loggerName, String filePath) {

		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();

		PatternLayoutEncoder ple = new PatternLayoutEncoder();

		ple.setPattern(DEFAULT_PATTERN_LAYOUT);
		ple.setContext(lc);
		ple.start();
		FileAppender<ILoggingEvent> fileAppender = new FileAppender<ILoggingEvent>();
		fileAppender.setFile(filePath + loggerName + ".log");
		fileAppender.setEncoder(ple);
		fileAppender.setContext(lc);
		fileAppender.start();

		Logger logger = (Logger) LoggerFactory.getLogger(loggerName);
		logger.addAppender(fileAppender);
		logger.setLevel(level);
		logger.setAdditive(false);

		return logger;
	}

	/**
	 * <pre>
	 * 주어진 파라메터에 맞는 Logger를 생성하여 반환한다.
	 * </pre>
	 *
	 * @param level
	 *            로그 레벨.
	 * @param loggerName
	 *            로거 이름.
	 * @param filePath
	 *            로그 기록 경로.
	 * @return
	 */
	public static Logger createRollingLogger(Level level, String loggerName, String filePath) {
		return createRollingLogger(level, loggerName, filePath, DEFAULT_PATTERN_LAYOUT,
				filePath + loggerName + "_%d{yyyyMMdd}.log");
	}

	/**
	 * <pre>
	 * 주어진 파라메터에 맞는 Logger를 생성하여 반환한다.
	 * </pre>
	 *
	 * @param level
	 *            로그 레벨.
	 * @param loggerName
	 *            로거 이름.
	 * @param filePath
	 *            로그 기록 경로.
	 * @param patternLayout
	 *            로깅 패턴 문자열
	 * @param rollingFileNamePattern
	 *            rolling 파일 명칭.
	 * @return
	 */
	public static Logger createRollingLogger(Level level, String loggerName, String filePath, String patternLayout,
			String rollingFileNamePattern) {

		LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

		PatternLayoutEncoder encoder = new PatternLayoutEncoder();

		encoder.setPattern(patternLayout);
		encoder.setContext(context);
		encoder.start();

		DefaultTimeBasedFileNamingAndTriggeringPolicy<ILoggingEvent> timeBasedTriggeringPolicy = new DefaultTimeBasedFileNamingAndTriggeringPolicy<ILoggingEvent>();
		timeBasedTriggeringPolicy.setContext(context);

		TimeBasedRollingPolicy<ILoggingEvent> timeBasedRollingPolicy = new TimeBasedRollingPolicy<ILoggingEvent>();
		timeBasedRollingPolicy.setContext(context);
		timeBasedRollingPolicy.setFileNamePattern(rollingFileNamePattern);
		timeBasedRollingPolicy.setTimeBasedFileNamingAndTriggeringPolicy(timeBasedTriggeringPolicy);
		timeBasedTriggeringPolicy.setTimeBasedRollingPolicy(timeBasedRollingPolicy);

		RollingFileAppender<ILoggingEvent> rollingFileAppender = new RollingFileAppender<ILoggingEvent>();
		rollingFileAppender.setAppend(true);
		rollingFileAppender.setContext(context);
		rollingFileAppender.setEncoder(encoder);
		rollingFileAppender.setFile(filePath + File.separator + loggerName + ".log");
		rollingFileAppender.setName(loggerName);
		rollingFileAppender.setPrudent(false);
		rollingFileAppender.setRollingPolicy(timeBasedRollingPolicy);
		rollingFileAppender.setTriggeringPolicy(timeBasedTriggeringPolicy);

		timeBasedRollingPolicy.setParent(rollingFileAppender);

		timeBasedRollingPolicy.start();

		rollingFileAppender.stop();
		rollingFileAppender.start();

		Logger logger = (Logger) LoggerFactory.getLogger(loggerName);
		logger.addAppender(rollingFileAppender);
		logger.setLevel(level);
		logger.setAdditive(false);

		return logger;
	}

	// public static final void main(String[] args) {
	// Logger logger = LoggerUtils.createRollingLogger(Level.INFO, "test", "c:\\");
	// Logger logger1 = LoggerUtils.createLogger(Level.INFO, "test1123", "c:\\");
	// logger.info("123");
	// logger1.info("qweqwe");
	// }
}
