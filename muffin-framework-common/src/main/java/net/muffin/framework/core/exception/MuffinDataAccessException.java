package net.muffin.framework.core.exception;

import net.muffin.framework.core.context.MuffinReturnCodeSpec;

public class MuffinDataAccessException extends MuffinException {

	private static final long serialVersionUID = 1L;

	private static final String ERROR_CODE = MuffinReturnCodeSpec.ERR_SYS_DB.code();

	/**
	 * <pre>
	 * Default Constructor.
	 * </pre>
	 */
	public MuffinDataAccessException() {
		this(ERROR_CODE);
	}

	/**
	 * <pre>
	 * Constructor. (Default ErrorCode = {@link MuffinReturnCodeSpec.ERR_SYS_DB}
	 * </pre>
	 *
	 * @param cause
	 *            예외 유발 정보
	 */
	public MuffinDataAccessException(Throwable cause) {
		super(ERROR_CODE, cause);
	}

	/**
	 * <pre>
	 * Constructor.
	 * </pre>
	 *
	 * @param code
	 *            예외 코드
	 */
	public MuffinDataAccessException(String code) {
		super(code, (Object[]) null);
	}

}
