package net.muffin.framework.core.persistence.dao.page;

public interface Paginate {

	/**
	 * <pre>
	 * PaginateInfo 조회.
	 * </pre>
	 * 
	 * @return PaginateInfo
	 */
	public PaginateInfo getPage();

	/**
	 * <pre>
	 * PaginateInfo 저장.
	 * </pre>
	 * 
	 * @param paginateInfo
	 */
	public void setPage(PaginateInfo paginateInfo);
}
