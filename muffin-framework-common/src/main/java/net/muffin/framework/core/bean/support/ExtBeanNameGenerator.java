package net.muffin.framework.core.bean.support;

import java.beans.Introspector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;

public class ExtBeanNameGenerator implements BeanNameGenerator {

	/** The logger. */
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.beans.factory.support.BeanNameGenerator#generateBeanName(org.springframework.beans.factory
	 * .config.BeanDefinition, org.springframework.beans.factory.support.BeanDefinitionRegistry)
	 */
	@Override
	public String generateBeanName(BeanDefinition definition, BeanDefinitionRegistry registry) {
		String beanName = Introspector.decapitalize(definition.getBeanClassName());
		this.logger.debug("### generateBeanName : {}", beanName);

		return beanName;
	}
}
