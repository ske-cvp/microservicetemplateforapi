package net.muffin.framework.core.persistence.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.muffin.framework.core.common.domain.PageParamWrapper;
import net.muffin.framework.core.exception.MuffinException;
import net.muffin.framework.core.persistence.dao.page.PageStatement;
import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import net.muffin.framework.core.persistence.dao.handler.StreamHandler;
import net.muffin.framework.core.persistence.dao.page.PageInfo;
import net.muffin.framework.core.persistence.dao.page.PaginateInfo;

@SuppressWarnings("rawtypes")
public class MyBatisCommonDao implements CommonDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(MyBatisCommonDao.class);

	@Autowired
	protected SqlSessionTemplate template;

	@Override
	public Integer queryForInt(String statementId, Object parameter) {
		Integer value = (Integer) template.selectOne(statementId, parameter);

		if (value == null) {
			value = 0;
		}

		return value;
	}

	@Override
	public Long queryForLong(String statementId, Object parameter) {
		return (Long) template.selectOne(statementId, parameter);
	}

	@Override
	public <T> T queryForObject(String statementId, Object parameter, Class<T> clazz) {
		return clazz.cast(template.selectOne(statementId, parameter));
	}

	@Override
	public Object queryForObject(String statementId, Object parameter) {
		return template.selectOne(statementId, parameter);
	}

	@Override
	public Map<?, ?> queryForMap(String statementId, String mapKey, Object parameter) {
		return template.selectMap(statementId, parameter, mapKey);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> List<T> queryForList(String statementId, Object parameter, Class<T> clazz) {
		List<T> list = (List<T>) template.selectList(statementId, parameter);

		LOGGER.debug("Statement[{}] Executed ({}) : {} Records retrieved.",
				new Object[] { statementId, new Date(), list == null ? 0 : list.size() });

		return list;
	}

	@Override
	public List<?> queryForList(String statementId, Object parameter) {
		List<?> list = template.selectList(statementId, parameter);

		LOGGER.debug("Statement[{}] Executed ({}) : {} Records retrieved.",
				new Object[] { statementId, new Date(), list == null ? 0 : list.size() });

		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> PageInfo<T> queryForPagenatedList(PageStatement statement, Object parameter) {
		int totalCount = queryForInt(statement.getTotalCountStatementId(), parameter);

		List<T> list = null;
		PaginateInfo paginateInfo = new PaginateInfo();

		try {
			paginateInfo = (PaginateInfo) (parameter.getClass().getMethod("getPage")).invoke(parameter);
			paginateInfo.setTotalCount(totalCount);
			(parameter.getClass().getMethod("setPage", PaginateInfo.class)).invoke(parameter, paginateInfo);
		} catch (Exception ex) {
			LOGGER.error("queryForPagenatedList 중 에러가 발생 했습니다. \\n{}", ex);
			throw new MuffinException("ValueObject : " + parameter.getClass() + "에 PaginateInfoVO 객체를 확인 하세요.", ex);
		}

		if (totalCount > 0) {
			list = (List<T>) template.selectList(statement.getDataStatementId(), parameter);
		}

		PageInfo<T> pageInfo = new PageInfo<T>(list == null ? new ArrayList<T>(0) : list);
		pageInfo.setPage(paginateInfo);

		return pageInfo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> PageInfo<T> queryForPagenatedList(PageStatement statement, Object parameter, int pageNum, int pageRows) {
		List<T> list = null;
		PageInfo<T> pageInfo = null;
		PageParamWrapper paramWrapper = new PageParamWrapper();
		PaginateInfo paginateInfo = new PaginateInfo();
		paramWrapper.setCondition(parameter);
		paramWrapper.setPage(paginateInfo);

		int totalCount = queryForInt(statement.getTotalCountStatementId(), paramWrapper);

		try {
			paginateInfo.setNo(pageNum == 0 ? 1 : pageNum);
			paginateInfo.setRows(pageRows);
			paginateInfo.setTotalCount(totalCount);
			// (parameter.getClass().getMethod("setPage", PaginateInfo.class)).invoke(parameter, paginateInfo);
		} catch (Exception ex) {
			LOGGER.error("queryForPagenatedList 중 에러가 발생 했습니다. \\n{}", ex);
			throw new MuffinException("ValueObject : " + parameter.getClass() + "에 PaginateInfoVO 객체를 확인 하세요.", ex);
		}

		if (totalCount > 0) {
			list = (List<T>) template.selectList(statement.getDataStatementId(), paramWrapper);

			LOGGER.debug("Statement[{}] Executed ({}) : {} Records retrieved.",
					new Object[] { statement.getDataStatementId(), new Date(), list == null ? 0 : list.size() });
		}

		pageInfo = new PageInfo<T>(list == null ? new ArrayList<T>(0) : list);
		pageInfo.setPage(paginateInfo);
		return pageInfo;
	}

	@Override
	public <T, R> List<R> queryWithResultHandler(String statementId, Object parameter,
			final StreamHandler<T, R> streamHandler) {
		streamHandler.open();

		try {
			final List<R> list = new ArrayList<R>();

			template.select(statementId, parameter, new ResultHandler() {
				@Override
				@SuppressWarnings("unchecked")
				public void handleResult(ResultContext context) {
					R result = streamHandler.handleRow((T) context.getResultObject());

					if (result != null) {
						list.add(result);
					}

					if (streamHandler.isStop()) {
						context.stop();
					}
				}
			});

			return list;
		} finally {
			streamHandler.close();
		}
	}

	@Override
	public <T, R> PageInfo<R> queryWithResultHandler(PageStatement statement, Object parameter, int pageNum,
			int pageRows, final StreamHandler<T, R> streamHandler) {
		streamHandler.open();

		try {
			int totalRowCount = queryForInt(statement.getTotalCountStatementId(), parameter);
			final List<R> list = new ArrayList<R>();

			if (totalRowCount > 0) {
				int startRows = (pageNum - 1) * pageRows;
				int endRows = pageRows;

				LOGGER.debug("Start={}, End={}", startRows, endRows);

				template.select(statement.getDataStatementId(), parameter, new RowBounds(startRows, endRows),
						new ResultHandler() {

							@SuppressWarnings("unchecked")
							@Override
							public void handleResult(ResultContext context) {
								R result = streamHandler.handleRow((T) context.getResultObject());

								if (result != null) {
									list.add(result);
								}

								if (streamHandler.isStop()) {
									context.stop();
								}
							}
						});
			}

			LOGGER.debug("Statement[{}] Executed ({}) : {} Records retrieved.",
					new Object[] { statement.getDataStatementId(), new Date(), list == null ? 0 : list.size() });

			return new PageInfo<R>(totalRowCount, list);

		} finally {
			streamHandler.close();
		}
	}

	@Override
	public Integer update(String statementId, Object parameter) {
		return template.update(statementId, parameter);
	}

	@Override
	public Integer insert(String statementId, Object parameter) {
		return template.insert(statementId, parameter);
	}

	@Override
	public Integer delete(String statementId, Object parameter) {
		return template.delete(statementId, parameter);
	}

	/**
	 * {@link SqlSessionTemplate}에 대한 의존성을 직접 주입.
	 *
	 * @param template
	 *            template
	 */
	public void setSqlSessionTemplate(SqlSessionTemplate template) {
		this.template = template;
	}
}
