package net.muffin.framework.core.util;

import java.util.UUID;

/**
 * <p>
 * 프로세스 아이디 생성 유틸 클래스.
 * </p>
 */
public class ProcessIDGenerator {

	synchronized public static String generateProcessId() {
		return System.currentTimeMillis() + UUID.randomUUID().toString();
	}
}
