package net.muffin.framework.core.persistence.dao.handler;

public abstract class RowHandler<T, R> implements StreamHandler<T, R> {

	private boolean stop = false;

	@Override
	public void open() {
	}

	@Override
	public void close() {
	}

	@Override
	public boolean isStop() {
		return this.stop;
	}

	@Override
	public void stop() {
		this.stop = true;
	}

	@Override
	public abstract R handleRow(T valueObject);
}
