package net.muffin.framework.core.exception;

import net.muffin.framework.core.context.MuffinReturnCodeSpec;
import net.muffin.framework.core.context.RequestType;
import org.apache.commons.lang3.StringUtils;

import net.muffin.framework.core.exception.domain.ErrorInfo;

public class MuffinLogoutException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private ErrorInfo errorInfo;
	private RequestType requestType;

	public MuffinLogoutException(ErrorInfo errorInfo) {
		super(errorInfo.getCode());
		this.errorInfo = errorInfo;
	}

	public MuffinLogoutException(ErrorInfo errorInfo, Throwable cause) {
		super(errorInfo.getCode(), cause);
		this.errorInfo = errorInfo;
	}

	public MuffinLogoutException(MuffinReturnCodeSpec codespec, RequestType requestType) {
		this(codespec, requestType, new Object());
	}

	public MuffinLogoutException(MuffinReturnCodeSpec codespec, RequestType requestType, Object... args) {
		super(codespec.code());
		this.requestType = requestType;
		this.errorInfo = new ErrorInfo();
		this.errorInfo.setCode(codespec.code());
		this.errorInfo.setArgs(args);
	}

	public ErrorInfo getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(ErrorInfo errorInfo) {
		this.errorInfo = errorInfo;
	}

	public String getCode() {
		if (errorInfo == null || StringUtils.isEmpty(errorInfo.getCode())) {
			return null;
		}
		return errorInfo.getCode();
	}

	public RequestType getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}

}
