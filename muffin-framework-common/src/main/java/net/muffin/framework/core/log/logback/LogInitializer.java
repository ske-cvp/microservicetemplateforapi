package net.muffin.framework.core.log.logback;

import java.net.UnknownHostException;
import java.util.List;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.classic.util.ContextInitializer;
import ch.qos.logback.core.joran.spi.JoranException;

public class LogInitializer {

	@SuppressWarnings("rawtypes")
	private List logbackPath;

	/**
	 * <pre>
	 * 초기화 메소드.
	 * </pre>
	 */
	public void initialize() {
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();

		lc.reset();

		try {
			try {
				lc.putProperty("hostName", java.net.InetAddress.getLocalHost().getHostName());
			} catch (UnknownHostException e) {
				e.printStackTrace();
				lc.putProperty("hostName", "UnknownHost");
			}

//			lc.putProperty("instanceName", System.getProperty("env.servername"));

			if (this.logbackPath != null && this.logbackPath.size() > 0) {
				JoranConfigurator joranConfigurator = new JoranConfigurator();
				joranConfigurator.setContext(lc);
				for (int index = 0; index < this.logbackPath.size(); index++) {
					joranConfigurator.doConfigure(this.getClass().getClassLoader()
							.getResourceAsStream(this.logbackPath.get(index).toString()));
				}
			}

			ContextInitializer ci = new ContextInitializer(lc);

			ci.autoConfig();

		} catch (JoranException e) {
			LoggerContext lc2 = (LoggerContext) LoggerFactory.getILoggerFactory();
			ContextInitializer ci = new ContextInitializer(lc2);
			try {
				ci.autoConfig();
			} catch (JoranException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}

	/**
	 * <pre>
	 * 완료 메소드.
	 * </pre>
	 */
	public void destroy() {
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		lc.stop();
	}

	/**
	 * 추가 설정 logbackPath.
	 *
	 * @param logbackPath
	 *            logbackPath
	 */
	@SuppressWarnings("rawtypes")
	public void setLogbackPath(List logbackPath) {
		this.logbackPath = logbackPath;
	}

}
