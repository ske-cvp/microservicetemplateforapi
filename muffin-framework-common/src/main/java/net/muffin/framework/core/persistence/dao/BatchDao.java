package net.muffin.framework.core.persistence.dao;

import java.util.List;

import org.apache.ibatis.executor.BatchResult;

public interface BatchDao extends CommonDao {
	/**
	 *
	 * <pre>
	 * 배치 작업 중 statement를 비우는 작업을 수행.
	 * </pre>
	 *
	 * @return
	 */
	List<BatchResult> flushStatements();
}
