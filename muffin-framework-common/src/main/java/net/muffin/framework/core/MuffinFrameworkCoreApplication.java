package net.muffin.framework.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MuffinFrameworkCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(MuffinFrameworkCoreApplication.class, args);
	}
}
