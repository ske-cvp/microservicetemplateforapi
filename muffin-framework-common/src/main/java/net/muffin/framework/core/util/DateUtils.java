package net.muffin.framework.core.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils extends org.apache.commons.lang3.time.DateUtils {

	/**
	 * <pre>
	 * 주어진 Milisecond 값을 주어진 Date Format에 맞게 변환한 문자열을 반환 한다.
	 * </pre>
	 *
	 * @param duration
	 *            경과 시간
	 * @param dateFormat
	 *            출력 일시 형식
	 * @return 일시 형식의 문자열
	 */
	public static String converDurationTime(long duration, String dateFormat) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(duration);
		// 시간 정보 초기화 작업.
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.DATE, 31);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		String timeStr = new SimpleDateFormat(dateFormat).format(cal.getTime());
		return timeStr;
	}

	/**
	 * <pre>
	 * 현재시각(yyyyMMddHHmmssSS).
	 * </pre>
	 *
	 * @return
	 */
	public static String currentTime() {

		Calendar cal = Calendar.getInstance();

		String timeStr = new SimpleDateFormat("yyyyMMddHHmmssSS").format(cal.getTime());

		return timeStr;

	}

	public static String currentTime(String dateFormat) {

		Calendar cal = Calendar.getInstance();

		String timeStr = null;

		try {
			timeStr = new SimpleDateFormat(dateFormat).format(cal.getTime());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return timeStr;
	}

	/**
	 * <pre>
	 * 현재시각(yyyyMMddHHmmss).
	 * </pre>
	 *
	 * @return
	 */
	public static String currentDate() {
		return currentTime("yyyyMMddHHmmss");
	}

	/**
	 * <pre>
	 * 현재날짜(yyyyMMdd).
	 * </pre>
	 *
	 * @return
	 */
	public static String currentDay() {
		return currentTime("yyyyMMdd");
	}

	/**
	 * <pre>
	 * 현재날짜(yyyyMM).
	 * </pre>
	 *
	 * @return
	 */
	public static String currentMonth() {
		return currentTime("yyyyMM");
	}

	public static Date addYear(Date date, int amount) {
		return addDate(date, 1, amount);
	}

	public static Date addMonth(Date date, int amount) {
		return addDate(date, 2, amount);
	}

	public static Date addDay(Date date, int amount) {
		return addDate(date, 5, amount);
	}

	public static Date addHour(Date date, int amount) {
		return addDate(date, 10, amount);
	}

	public static Date addMinute(Date date, int amount) {
		return addDate(date, 12, amount);
	}

	public static Date addSecond(Date date, int amount) {
		return addDate(date, 13, amount);
	}

	private static final Date addDate(Date date, int field, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(field, amount);
		return cal.getTime();
	}

}
