package net.muffin.framework.core.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonUtils {

	/**
	 *
	 * <pre>
	 * 주어진 데이터를 Json 형태의 문자열로 반환한다. (
	 * </pre>
	 *
	 * @param data
	 *            처리 대상 데이터
	 * @param prettyPrint
	 *            pretty 출력 여부
	 * @return data 기준 Json 문자열
	 * @throws JsonGenerationException
	 *             Json 처리시에 예외 상황이 발생한 경우.
	 * @throws JsonMappingException
	 *             Object Mapping 처리시 예외 상황이 발생하는 경우.
	 * @throws IOException
	 *             출력 처리시에 예외 상황이 발생하는 경우.
	 */
	public static String printUnwrapRootName(Object data)
			throws JsonGenerationException, JsonMappingException, IOException {
		return printUnwrapRootName(data, false);
	}

	/**
	 *
	 * <pre>
	 * 주어진 데이터를 Json 형태의 문자열로 반환한다. (
	 * </pre>
	 *
	 * @param data
	 *            처리 대상 데이터
	 * @param prettyPrint
	 *            pretty 출력 여부
	 * @return data 기준 Json 문자열
	 * @throws JsonGenerationException
	 *             Json 처리시에 예외 상황이 발생한 경우.
	 * @throws JsonMappingException
	 *             Object Mapping 처리시 예외 상황이 발생하는 경우.
	 * @throws IOException
	 *             출력 처리시에 예외 상황이 발생하는 경우.
	 */
	public static String printUnwrapRootName(Object data, boolean prettyPrint)
			throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
		mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
		return print(mapper, data, prettyPrint);
	}

	/**
	 * <pre>
	 * 주어진 데이터를 OutputStream에 Json 형태로 출력처리를 수행 한다. (prettyPrint=false)
	 * </pre>
	 *
	 * @param out
	 *            출력 대상 OutputStream
	 * @param data
	 *            출력 데이터
	 * @param prettyPrint
	 *            pretty 출력 여부
	 * @throws JsonGenerationException
	 *             Json 처리시에 예외 상황이 발생한 경우.
	 * @throws JsonMappingException
	 *             Object Mapping 처리시 예외 상황이 발생하는 경우.
	 * @throws IOException
	 *             출력 처리시에 예외 상황이 발생하는 경우.
	 */
	public static void print(OutputStream out, Object data)
			throws JsonGenerationException, JsonMappingException, IOException {
		print(out, data, false);
	}

	/**
	 * <pre>
	 * 주어진 데이터를 OutputStream에 Json 형태로 출력처리를 수행 한다.
	 * </pre>
	 *
	 * @param out
	 *            출력 대상 OutputStream
	 * @param data
	 *            출력 데이터
	 * @param prettyPrint
	 *            pretty 출력 여부
	 * @throws JsonGenerationException
	 *             Json 처리시에 예외 상황이 발생한 경우.
	 * @throws JsonMappingException
	 *             Object Mapping 처리시 예외 상황이 발생하는 경우.
	 * @throws IOException
	 *             출력 처리시에 예외 상황이 발생하는 경우.
	 */
	public static void print(OutputStream out, Object data, boolean prettyPrint)
			throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		print(mapper, out, data, prettyPrint);
	}

	/**
	 * <pre>
	 * 주어진 데이터를 OutputStream에 Json 형태로 출력처리를 수행 한다.
	 * </pre>
	 *
	 * @param mapper
	 *            Json Object Mapper
	 * @param out
	 *            출력 대상 OutputStream
	 * @param data
	 *            출력 데이터
	 * @param prettyPrint
	 *            pretty 출력 여부
	 * @throws JsonGenerationException
	 *             Json 처리시에 예외 상황이 발생한 경우.
	 * @throws JsonMappingException
	 *             Object Mapping 처리시 예외 상황이 발생하는 경우.
	 * @throws IOException
	 *             출력 처리시에 예외 상황이 발생하는 경우.
	 */
	public static void print(ObjectMapper mapper, OutputStream out, Object data, boolean prettyPrint)
			throws JsonGenerationException, JsonMappingException, IOException {
		if (prettyPrint) {
			mapper.writerWithDefaultPrettyPrinter().writeValue(out, data);
		} else {
			mapper.writeValue(out, data);
		}
	}

	/**
	 *
	 * <pre>
	 * 주어진 데이터를 Json 형태의 문자열로 반환한다. (prettyPrint=false)
	 * </pre>
	 *
	 * @param data
	 *            처리 대상 데이터
	 * @return data 기준 Json 문자열
	 * @throws JsonGenerationException
	 *             Json 처리시에 예외 상황이 발생한 경우.
	 * @throws JsonMappingException
	 *             Object Mapping 처리시 예외 상황이 발생하는 경우.
	 * @throws IOException
	 *             출력 처리시에 예외 상황이 발생하는 경우.
	 */
	public static String print(Object data) throws JsonGenerationException, JsonMappingException, IOException {
		return print(data, false);
	}

	/**
	 *
	 * <pre>
	 * 주어진 데이터를 Json 형태의 문자열로 반환한다.
	 * </pre>
	 *
	 * @param data
	 *            처리 대상 데이터
	 * @param prettyPrint
	 *            pretty 출력 여부
	 * @return data 기준 Json 문자열
	 * @throws JsonGenerationException
	 *             Json 처리시에 예외 상황이 발생한 경우.
	 * @throws JsonMappingException
	 *             Object Mapping 처리시 예외 상황이 발생하는 경우.
	 * @throws IOException
	 *             출력 처리시에 예외 상황이 발생하는 경우.
	 */
	public static String print(Object data, boolean prettyPrint)
			throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return print(mapper, data, prettyPrint);
	}

	/**
	 *
	 * <pre>
	 * 주어진 데이터를 Json 형태의 문자열로 반환한다.
	 * </pre>
	 *
	 * @param mapper
	 *            Json Object Mapper
	 * @param data
	 *            처리 대상 데이터
	 * @param prettyPrint
	 *            pretty 출력 여부
	 * @return data 기준 Json 문자열
	 * @throws JsonGenerationException
	 *             Json 처리시에 예외 상황이 발생한 경우.
	 * @throws JsonMappingException
	 *             Object Mapping 처리시 예외 상황이 발생하는 경우.
	 * @throws IOException
	 *             출력 처리시에 예외 상황이 발생하는 경우.
	 */
	public static String print(ObjectMapper mapper, Object data, boolean prettyPrint)
			throws JsonGenerationException, JsonMappingException, IOException {
		if (prettyPrint) {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(data);
		}
		return mapper.writeValueAsString(data);
	}

	/**
	 *
	 * <pre>
	 * 주어진 json 형태 문자열을 객체로 반환환한다.
	 * </pre>
	 *
	 * @param json
	 *            Json 형태 문자열
	 * @param clazz
	 *            반환 기준 class
	 * @return clazz type Object
	 * @throws JsonParseException
	 *             Parsing 처리 시 예외 상황이 발생하는 경우
	 * @throws JsonMappingException
	 *             Object Mapping 처리시 예외 상황이 발생하는 경우.
	 * @throws IOException
	 *             출력 처리 시에 예외 상황이 발생하는 경우.
	 */
	public static <T> T convert(String json, Class<T> clazz)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, clazz);
	}

	/**
	 *
	 * <pre>
	 * 주어진 json 형태 문자열을 객체로 반환환한다.
	 * </pre>
	 *
	 * @param json
	 *            Json 형태 문자열
	 * @param clazz
	 *            반환 기준 class
	 * @return clazz type Object
	 * @throws JsonParseException
	 *             Parsing 처리 시 예외 상황이 발생하는 경우
	 * @throws JsonMappingException
	 *             Object Mapping 처리시 예외 상황이 발생하는 경우.
	 * @throws IOException
	 *             출력 처리 시에 예외 상황이 발생하는 경우.
	 */
	public static <T> T convert(String json, TypeReference<T> type)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, type);
	}

	/**
	 *
	 * <pre>
	 * 주어진 json 형태 문자열을 Map<String, Object> 객체로 반환환한다.
	 * </pre>
	 *
	 * @param json
	 *            Json 형태 문자열
	 * @return Map type Object
	 * @throws JsonParseException
	 *             Parsing 처리 시 예외 상황이 발생하는 경우
	 * @throws JsonMappingException
	 *             Object Mapping 처리시 예외 상황이 발생하는 경우.
	 * @throws IOException
	 *             출력 처리 시에 예외 상황이 발생하는 경우.
	 */
	public static Map<String, Object> convertToMap(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, new TypeReference<HashMap<String, Object>>() {
		});
	}
}
