package net.muffin.framework.core.log.omc;

import org.slf4j.Logger;

/**
 * <p>외부 연동 감시를 위한 OMC로그 정보 모델 클래스.</p>
 *
 * <ul>
 * <li>Created on : 2017. 7. 24.</li>
 * <li>Created by : sptek</li>
 * </ul>
 */
public class OmcLogUtils {

	/**
	 * <pre>
	 * 외부 연동 감시를 위한 OMC로그 정보를 기록한다.
	 * </pre>
	 *
	 * @param logger
	 *            로그 기록 객체
	 * @param context
	 *            외부 연동 감시 결과 정보 객체
	 */
	public static void log(Logger logger, OmcContext context) {
		if (context != null && context.isLoggable()) {
			logger.info("{};{};{};{};{};{};{};{};{};{};{};{}", context.getHostNm(), context.getRemoteSystemNm(),
					context.getOpResult().code(), context.getResultCode(), context.getDuration(),
					context.getServiceNm(), context.getRequestTimeAsString(), context.getResponseTimeAsString(),
					context.getRemotePrimaryKey(), context.getRemoteSecondaryKey(), context.getRemoteURL(),
					context.getExtraAsString());
		}
	}
}
