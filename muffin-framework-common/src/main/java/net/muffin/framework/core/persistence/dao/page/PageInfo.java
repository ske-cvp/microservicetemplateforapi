package net.muffin.framework.core.persistence.dao.page;

import java.util.List;

public class PageInfo<T> implements Paginate {

	public PageInfo(List<T> data) {
		this.data = data;
	}

	public PageInfo(int totalCount, List<T> data) {
		if (this.page == null)
			this.page = new PaginateInfo();
		this.data = data;
		this.page.setTotalCount(totalCount);
	}

	private PaginateInfo page = new PaginateInfo();

	@Override
	public PaginateInfo getPage() {
		return this.page;
	}

	@Override
	public void setPage(PaginateInfo paginateInfo) {
		this.page = paginateInfo;
	}

	private List<T> data;

	public int getTotalRowCount() {
		return this.page.getTotalCount();
	}

	public List<T> getData() {
		return this.data;
	}
}
