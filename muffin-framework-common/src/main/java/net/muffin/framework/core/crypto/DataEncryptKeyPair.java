package net.muffin.framework.core.crypto;

import lombok.Data;

/**
 * <p>데이터 암호화 시 사용되는 대칭키&키인덱스 모델</p>
 *
 * <ul>
 * <li>Created on : 2017. 6. 8.</li>
 * <li>Created by : sptek</li>
 * </ul>
 */
@Data
public class DataEncryptKeyPair {

	private String key;

	private int keyIdx;

	public DataEncryptKeyPair(String key, int keyIdx) {
		this.key = key;
		this.keyIdx = keyIdx;
	}
}
