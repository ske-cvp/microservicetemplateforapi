package net.muffin.framework.core.common.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import net.muffin.framework.core.persistence.dao.page.Paginate;
import net.muffin.framework.core.persistence.dao.page.PaginateInfo;

@SuppressWarnings("serial")
public abstract class PaginateVo extends CommonVo implements Paginate {

	@JsonInclude(Include.NON_NULL)
	protected PaginateInfo page = null;

	@Override
	public PaginateInfo getPage() {
		return this.page;
	}

	@Override
	public void setPage(PaginateInfo pim) {
		this.page = pim;
	}

}
