package net.muffin.framework.core.crypto.asymmetric;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;

import net.muffin.framework.core.context.MuffinReturnCodeSpec;
import net.muffin.framework.core.exception.MuffinException;

/**
 * <p>RSA 암복호화. 공개키&비밀키 생성</p>
 *
 * <ul>
 * <li>Created on : 2017. 6. 8.</li>
 * <li>Created by : sptek</li>
 * </ul>
 */
public class RsaEncryptor {
	
	String mode = "RSA";
	
	public RsaEncryptor() {}
	
	public RsaEncryptor(String mode) {
		this.mode = mode;
	}	
	/**
	 * <pre>RSA공개키문자열을 PublicKey객체로 변환.</pre>
	 * @param rsaPubStr
	 * @return PublicKey
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	private PublicKey getRsaPublicKey(String rsaPubStr) throws NoSuchAlgorithmException, InvalidKeySpecException {
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.decodeBase64(rsaPubStr));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePublic(keySpec);
	}

	/**
	 * <pre>RSA비밀키문자열을 PrivateKey객체로 변환.</pre>
	 * @param rsaPrvStr
	 * @return PrivateKey
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	private PrivateKey getRsaPrivateKey(String rsaPrvStr) throws NoSuchAlgorithmException, InvalidKeySpecException {
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(rsaPrvStr));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePrivate(keySpec);
	}

	/**
	 * <pre>RSA공개키로 대상 암호화.</pre>
	 * @param source
	 * @param rsaPubStr
	 * @return encryptedDataString
	 */
	public String rsaEncrypt(String source, String rsaPubStr) {
		try {
			PublicKey publicKey = this.getRsaPublicKey(rsaPubStr);
			Cipher cipher = Cipher.getInstance(this.mode);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] encryptedData = cipher.doFinal(source.getBytes("UTF-8"));
            return Base64.encodeBase64String(encryptedData);
		} catch (Exception e) {
			throw new MuffinException(MuffinReturnCodeSpec.ERR_FRWK_CRYTO.code(), e.getCause(), e.getMessage());
		}
	}

	/**
	 * <pre>RSA비밀키로 암호화되어있는 대상 복호화.</pre>
	 * @param encrypted
	 * @param rsaPrvStr
	 * @return
	 */
	public String rsaDecrypt(String encrypted, String rsaPrvStr) {
		try {
			byte[] encryptedData = Base64.decodeBase64(encrypted);
			PrivateKey privateKey = this.getRsaPrivateKey(rsaPrvStr);
			Cipher cipher = Cipher.getInstance(this.mode);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] decryptedData = cipher.doFinal(encryptedData);
            return new String(decryptedData, "UTF-8");
		} catch (Exception e) {
			throw new MuffinException(MuffinReturnCodeSpec.ERR_FRWK_CRYTO.code(), e.getCause(), e.getMessage());
		}
	}
/*
	public static void main(String[] args) throws Exception {

		// 공개키&비밀키 생성
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(2048);

		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		PublicKey publicKey = keyPair.getPublic();
		PrivateKey privateKey = keyPair.getPrivate();

		String rsaPubStr = Base64.encodeBase64String(publicKey.getEncoded());
        String rsaPrvStr = Base64.encodeBase64String(privateKey.getEncoded());

        System.out.println("공개키="+rsaPubStr);
        System.out.println("비밀키="+rsaPrvStr);

        String source = "a가1A!b나2B@c다3B#d라4D$a가1A!b나2B@c다3B#d라4D$a가1A!b나2B@c다3B#d라4D$a가1A!b나2B@c다3B#d라4D$a가1A!b나2B@c다3B#d라4D$a가1A!b나2B@c다3B#d라4D$a가1A!b나2B@c다3B#d라4D$a가1A!b나2B@c다3B#d라4D$";
        System.out.println("원   문="+source);

        // 암호화
        RsaEncryptor encryptor = new RsaEncryptor();
        String encrypted = encryptor.rsaEncrypt(source, rsaPubStr);
        System.out.println("암호화="+encrypted);

        // 복호화
        System.out.println("복호화="+encryptor.rsaDecrypt(encrypted, rsaPrvStr));
	}
*/
}

